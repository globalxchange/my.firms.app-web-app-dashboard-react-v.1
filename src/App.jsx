// Package Imports
import { useState, useEffect, useContext } from "react";
import { BrowserRouter, Routes, Route, Navigate } from "react-router-dom";

// CSS Imports
import "./App.scss";
import "react-loading-skeleton/dist/skeleton.css";
import "react-toastify/dist/ReactToastify.css";

// Context Imports
import { GlobalContex } from "./globalContex";

// Image Imports
import Meta from "./static/images/icons/meta.svg";
import Banker from "./static/images/menulogos/Banker.svg";
import Capitalized from "./static/images/menulogos/Capitalized.svg";
import Create from "./static/images/menulogos/Create.svg";
import FundManager from "./static/images/menulogos/FundManager.svg";
import OTCDesks from "./static/images/menulogos/OtcDesks.svg";
import Terminals from "./static/images/menulogos/Terminals.svg";

import meta_full from "./static/images/logos/mverse_full.svg";
import banker_full from "./static/images/logos/banker_full.svg";
import cap_full from "./static/images/logos/cap_full.svg";
import create_full from "./static/images/logos/create_full.svg";
import otc_full from "./static/images/logos/otc_full.svg";
import terminals_full from "./static/images/logos/terminals_full.svg";
import funds_full from "./static/images/logos/funds_full.svg";

import crm from "./static/images/sidebarIcons/crm.svg";
import dash from "./static/images/sidebarIcons/dash.svg";
import affiliates from "./static/images/sidebarIcons/affiliates.svg";
import vaults from "./static/images/sidebarIcons/vaults.svg";
import terminal from "./static/images/sidebarIcons/terminal.svg";
import bonds from "./static/images/sidebarIcons/bonds.svg";
import loans from "./static/images/sidebarIcons/socially.svg";
import Lock from "./static/images/icons/lock.svg";
import defaultImg from "./static/images/icons/app_placeholder.png";

import { ReactComponent as Collapse_img } from "./static/images/icons/collapse.svg";
import { ReactComponent as Collapse1_img } from "./static/images/icons/collapse1.svg";
import Images from "./static/images/theimages/0-exporter";
// taxlogoico.svg
import TaxLogoIcon from "./static/images/theimages/taxlogoico.svg";
import AgencyLogoIcon from "./static/images/theimages/theagencymain.svg";
import AccountsLogoIcon from "./static/images/theimages/accountsmainicon.svg";
import LegalLogoIcon from "./static/images/theimages/legalmainicon.svg";

//cPage/Component Imports
import SignInPage from "./pages/SignInPage";
import HomePage from "./pages/HomePage";
import GlobalSidebar from "./globalComponents/GlobalSidebar";
// import Elements from "./pages/Elements";
import axios from "axios";

function App() {
  const [loginData, setLoginData] = useState(null);
  const [login, setLogin] = useState(false);
  const [collapse, setCollapse] = useState(false);
  const [selectedApp, setSelectedApp] = useState(null);
  const [modalOpen, setModalOpen] = useState(false);
  const [tabs, setTabs] = useState([]);
  const [selectedTab, setSelectedTab] = useState(null);
  const [banker, setBanker] = useState(null);
  const [bankerEmail, setBankerEmail] = useState("");
  const [bankerTag, setBankerTag] = useState("");
  const [allBankers, setAllBankers] = useState([]);
  const [allCoins, setAllCoins] = useState([]);
  const [selectedCoin, setSelectedCoin] = useState(null);

  const [selectedFilter1, setSelectedFilter1] = useState(null);
  const [selectedFilter2, setSelectedFilter2] = useState("");
  const [selectedFilter21, setSelectedFilter21] = useState("");
  const [filter1, setFilter1] = useState(false);
  const [filter2, setFilter2] = useState(false);
  const [customerEmailFilter, setCustomerEmailFilter] = useState(null);
  const [openCoinFilter, setOpenCoinFilter] = useState(false);

  const [globalMenuAdd, setGlobalMenuAdd] = useState(false);

  const [firmsDirectoryUser, setFirmsDirectoryUser] = useState();
  const [firmsDirectoryFirms, setFirmsDirectoryFirms] = useState();

  const globalMenu = [
    {
      appName: "Firms.App",
      appLogo: Images.icon1,
      appFullLogo: Images.FirmsFullLogo,
      appColor: "#021842",
    },
    {
      appName: "Accountants",
      appLogo: Images.icon2,
      appFullLogo: AccountsLogoIcon,
      appColor: "rgb(31, 48, 79)",
    },
    {
      appName: "Lawyer.Agency",
      appLogo: Images.icon3,
      appFullLogo: Images.LawyerFullLogo,
      appColor: "#334480",
    },
    {
      appName: "Cabinets ",
      appLogo: Images.icon4,
      appFullLogo: Images.CabinetFullLogo,
      appColor: "#5ABEA0",
    },
    {
      appName: "Chats ",
      appLogo: Images.icon5,
      appFullLogo: Images.ChatsFullLogo,
      appColor: "#166AB3",
    },
  ];

  const localMenu = [
    {
      menuName: "Dashboard",
      menuIcon: dash,
      enabled: true,
    },
    {
      menuName: "Bonds",
      menuIcon: bonds,
      enabled: true,
    },
    {
      menuName: "Loans",
      menuIcon: loans,
      enabled: false,
    },
    {
      menuName: "CRM",
      menuIcon: crm,
      enabled: true,
    },
    {
      menuName: "Affilaites",
      menuIcon: affiliates,
      enabled: false,
    },
    {
      menuName: "Terminal",
      menuIcon: terminal,
      enabled: false,
    },
  ];

  // useEffect(() => {
  //   setSelectedApp(globalMenu[0]);
  // }, []);

  useEffect(() => {
    if (localStorage.getItem("selectedApp") && selectedApp === null) {
      setSelectedApp(JSON.parse(localStorage.getItem("selectedApp")));
    } else if (localStorage.getItem("selectedApp")) {
      localStorage.setItem("selectedApp", JSON.stringify(selectedApp));
    } else {
      localStorage.setItem(
        "selectedApp",
        JSON.stringify({
          appName: "Firms.App",
          appLogo: Images.icon1,
          appFullLogo: Images.FirmsFullLogo,
          appColor: "#021842",
        })
      );
      setSelectedApp({
        appName: "Firms.App",
        appLogo: Images.icon1,
        appFullLogo: Images.FirmsFullLogo,
        appColor: "#021842",
      });
    }
  }, [selectedApp]);

  useEffect(() => {
    if (localStorage.getItem("loginData")) {
      setLoginData(JSON.parse(localStorage.getItem("loginData")));
    }
  }, [localStorage.getItem("loginData")]);

  useEffect(() => {
    if (localStorage.getItem("bankerEmailNew")) {
      setBankerEmail(localStorage.getItem("bankerEmailNew"));
    } else {
      setBankerEmail(loginData?.user?.email);
    }
    setSelectedApp(JSON.parse(localStorage.getItem("selectedApp")));
  }, []);

  useEffect(() => {
    axios
      .get(`https://comms.globalxchange.com/coin/vault/get/all/coins`)
      .then((res) => {
        if (res.data.status) {
          setAllCoins(res.data.coins);
          setSelectedCoin({
            coinImage:
              "https://apimachine-s3.s3.us-east-2.amazonaws.com/coinImages/dollar.png",
            coinName: "US Dollar",
            coinSymbol: "USD",
            market_cap: 0,
            symbol: "$",
            type: "fiat",
            usd_price: 1,
            volume_24hr: 0,
            _24hrchange: 0,
            _id: "5f21042d0562332558c93180",
          });
        }
      });
  }, []);

  const value = {
    globalMenu,
    localMenu,
    collapse,
    setCollapse,
    selectedApp,
    setSelectedApp,
    modalOpen,
    setModalOpen,
    tabs,
    setTabs,
    selectedTab,
    setSelectedTab,
    loginData,
    setLoginData,
    bankerTag,
    setBankerTag,
    login,
    setLogin,
    Lock,
    Collapse_img,
    Collapse1_img,
    defaultImg,
    allBankers,
    setAllBankers,
    bankerEmail,
    setBankerEmail,
    allCoins,
    setAllCoins,
    selectedCoin,
    setSelectedCoin,
    selectedFilter1,
    setSelectedFilter1,
    selectedFilter2,
    setSelectedFilter2,
    selectedFilter21,
    setSelectedFilter21,
    filter1,
    setFilter1,
    filter2,
    setFilter2,
    customerEmailFilter,
    setCustomerEmailFilter,
    openCoinFilter,
    setOpenCoinFilter,
    globalMenuAdd,
    setGlobalMenuAdd,
    firmsDirectoryUser,
    setFirmsDirectoryUser,
    firmsDirectoryFirms,
    setFirmsDirectoryFirms,
  };

  return (
    <div>
      <GlobalContex.Provider value={value}>
        <BrowserRouter>
          <Routes>
            {/* <Route path="/" element={<Elements />} /> */}
            <Route
              path="/example"
              element={
                loginData !== null ? (
                  <Navigate to={`/${selectedApp?.appName}`} />
                ) : (
                  <SignInPage />
                )
              }
            />
            <Route
              path="/login"
              element={
                loginData !== null ? (
                  <Navigate to={`/${selectedApp?.appName}`} />
                ) : (
                  <SignInPage />
                )
              }
            />
            {/* <Route
              path="/*"
              element={loginData !== null ? <HomePage /> : <Elements />}
            /> */}
            <Route
              path="/*"
              element={
                loginData !== null ? <HomePage /> : <Navigate to="/login" />
              }
            />
            {/* <Route
              path="*"
              element={loginData !== null ? <HomePage /> : <SignInPage />}
            /> */}
          </Routes>
        </BrowserRouter>
      </GlobalContex.Provider>
    </div>
  );
}

export default App;
