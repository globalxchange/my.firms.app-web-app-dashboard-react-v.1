import React from "react";
import { useContext } from "react";
import GlobalSidebar from "../../globalComponents/GlobalSidebar";
import LocalSidebar from "../../globalComponents/LocalSidebar";
import { GlobalContex } from "../../globalContex";

import TabsLayout from "../../globalComponents/TabsLayout";
import AdminModal from "../../globalComponents/AdminModal";

import crm from "../../static/images/sidebarIcons/crm.svg";
import dash from "../../static/images/sidebarIcons/dash.svg";
import affiliates from "../../static/images/sidebarIcons/affiliates.svg";
import vaults from "../../static/images/sidebarIcons/vaults.svg";
import terminal from "../../static/images/sidebarIcons/terminal.svg";
import bonds from "../../static/images/sidebarIcons/bonds.svg";
import loans from "../../static/images/sidebarIcons/socially.svg";
import Lock from "../../static/images/icons/lock.svg";
import defaultImg from "../../static/images/icons/app_placeholder.png";
import AgencyDashboard from "./AgencyDashboard";
import VaultPage from "../../globalComponents/VaultPage";
import Images from "../../static/images/theimages/0-exporter"
import CalendarIcon from "../../static/images/theimages/calicon.svg"
import ChatsIcon from "../../static/images/theimages/chatsicon.svg"
import CalculatorIco from "../../static/images/theimages/calcicon.svg"
import TaxFundIco from "../../static/images/theimages/taxfundico.svg"
import VauldIco from "../../static/images/theimages/vauldico.svg"
import ReceiptIco from "../../static/images/theimages/receiptico.svg"
import TaxedIco from "../../static/images/theimages/taxedico.svg"
import GovIcon from "../../static/images/theimages/govicon.svg"
import VaultIcon from "../../static/images/theimages/vaulticon.svg"
import DashIcon from "../../static/images/theimages/dasheico.svg";
import ServiceIcon from "../../static/images/theimages/serviceicon.svg";
import CRMIcon from "../../static/images/theimages/crmicon.svg";
import CalIcon from "../../static/images/theimages/calenico.svg";
import AffIcon from "../../static/images/theimages/afficon.svg";
import PublicIcon from "../../static/images/theimages/publicico.svg";
import BranchIcon from "../../static/images/theimages/branchico.svg";
import VauldIcon from "../../static/images/theimages/vauldicon.svg";
import InteIcon from "../../static/images/theimages/inteicon.svg";

const Agency = () => {
  const {
    collapse,
    setTabs,
    setSelectedTab,
    selectedTab,
    loginData,
    setBankerEmail,
    modalOpen,
    setModalOpen,
    localMenu,
    globalMenu,
  } = useContext(GlobalContex);

  const bankerMenu = [
    {
      menuName: "Dashboard",
      menuIcon: DashIcon,
      enabled: true,
    },
    {
      menuName: "Services",
      menuIcon: ServiceIcon,
      enabled: true,
    },
    {
      menuName: "CRM",
      menuIcon: CRMIcon,
      enabled: true,
    },
    {
      menuName: "Chats",
      menuIcon: ChatsIcon,
      enabled: true,
    },
    {
      menuName: "Affiliates",
      menuIcon: AffIcon,
      enabled: true,
    },
    {
      menuName: "Publications",
      menuIcon: PublicIcon,
      enabled: true,
    },
    {
      menuName: "Branches",
      menuIcon: BranchIcon,
      enabled: true,
    },
    {
      menuName: "Vauld",
      menuIcon: VauldIcon,
      enabled: true,
    },
    {
      menuName: "Integrations",
      menuIcon: InteIcon,
      enabled: true,
    }
  ];
  const conditionalPages = () => {
    console.log(selectedTab?.menuName, "khedhwedk");

    switch (selectedTab?.menuName) {
      case "Dashboard":
        return <AgencyDashboard />;
      case "Vaults":
        return <VaultPage />;
      default:
        return (
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              fontWeight: 700,
              fontSize: "20px",
              height: "70vh",
            }}
          >
            Coming Soon
          </div>
        );
        break;
    }
  };

  return (
    <>
      <div className={collapse ? "grid-cap-collapsed" : "grid-cap"}>
        <GlobalSidebar globalMenu={globalMenu} />
        <LocalSidebar localMenu={bankerMenu} />
        <div
          style={{
            display: "flex",
            flexDirection: "column",
          }}
        >
          <TabsLayout />
          <div style={{ flexGrow: 1 }}>{conditionalPages()}</div>
        </div>
      </div>
      {modalOpen && (
        <AdminModal
          onClose={() => setModalOpen(false)}
          onSuccess={() => setModalOpen(false)}
        />
      )}
    </>
  );
};

export default Agency;
