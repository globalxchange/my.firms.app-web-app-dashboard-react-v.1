import React from "react";
import { useContext } from "react";
import GlobalSidebar from "../../globalComponents/GlobalSidebar";
import LocalSidebar from "../../globalComponents/LocalSidebar";
import { GlobalContex } from "../../globalContex";

import TabsLayout from "../../globalComponents/TabsLayout";
import AdminModal from "../../globalComponents/AdminModal";

import crm from "../../static/images/sidebarIcons/crm.svg";
import dash from "../../static/images/sidebarIcons/dash.svg";
import affiliates from "../../static/images/sidebarIcons/affiliates.svg";
import vaults from "../../static/images/sidebarIcons/vaults.svg";
import terminal from "../../static/images/sidebarIcons/terminal.svg";
import bonds from "../../static/images/sidebarIcons/bonds.svg";
import loans from "../../static/images/sidebarIcons/socially.svg";
import Lock from "../../static/images/icons/lock.svg";
import defaultImg from "../../static/images/icons/app_placeholder.png";
import BankerDashboard from "./BankerDashboard";
import VaultPage from "../../globalComponents/VaultPage";

const Banker = () => {
  const {
    collapse,
    setTabs,
    setSelectedTab,
    selectedTab,
    loginData,
    setBankerEmail,
    modalOpen,
    setModalOpen,
    localMenu,
    globalMenu,
  } = useContext(GlobalContex);

  const bankerMenu = [
    {
      menuName: "Dashboard",
      menuIcon: dash,
      enabled: true,
    },
    {
      menuName: "Bonds",
      menuIcon: bonds,
      enabled: true,
    },
    {
      menuName: "Loans",
      menuIcon: loans,
      enabled: false,
    },
    {
      menuName: "CRM",
      menuIcon: crm,
      enabled: true,
    },
    {
      menuName: "Affilaites",
      menuIcon: affiliates,
      enabled: false,
    },
    {
      menuName: "Terminal",
      menuIcon: terminal,
      enabled: false,
    },
  ];

  const conditionalPages = () => {
    console.log(selectedTab?.menuName, "khedhwedk");

    switch (selectedTab?.menuName) {
      case "Dashboard":
        return <BankerDashboard />;
      case "Vaults":
        return <VaultPage />;
      case "Bonds":
        return (
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              fontWeight: 700,
              fontSize: "20px",
              height: "70vh",
            }}
          >
            This is Banker Bonds
          </div>
        );
      default:
        return (
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              fontWeight: 700,
              fontSize: "20px",
              height: "70vh",
            }}
          >
            Coming Soon
          </div>
        );
        break;
    }
  };

  return (
    <>
      <div className={collapse ? "grid-cap-collapsed" : "grid-cap"}>
        <GlobalSidebar globalMenu={globalMenu} />
        <LocalSidebar localMenu={bankerMenu} />
        <div
          style={{
            display: "flex",
            flexDirection: "column",
          }}
        >
          <TabsLayout />
          <div style={{ flexGrow: 1 }}>{conditionalPages()}</div>
        </div>
      </div>
      {modalOpen && (
        <AdminModal
          onClose={() => setModalOpen(false)}
          onSuccess={() => setModalOpen(false)}
        />
      )}
    </>
  );
};

export default Banker;
