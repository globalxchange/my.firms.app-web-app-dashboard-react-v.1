import React from "react";
import { useContext } from "react";
import GlobalSidebar from "../../globalComponents/GlobalSidebar";
import LocalSidebar from "../../globalComponents/LocalSidebar";
import { GlobalContex } from "../../globalContex";
import TabsLayout from "../../globalComponents/TabsLayout";

import crm from "../../static/images/sidebarIcons/crm.svg";
import dash from "../../static/images/sidebarIcons/dash.svg";
import affiliates from "../../static/images/sidebarIcons/affiliates.svg";
import vaults from "../../static/images/sidebarIcons/vaults.svg";
import terminal from "../../static/images/sidebarIcons/terminal.svg";
import bonds from "../../static/images/sidebarIcons/bonds.svg";
import loans from "../../static/images/sidebarIcons/socially.svg";
import Lock from "../../static/images/icons/lock.svg";
import defaultImg from "../../static/images/icons/app_placeholder.png";

import AdminModal from "../../globalComponents/AdminModal";

const Capitalized = () => {
  const {
    collapse,
    setTabs,
    setSelectedTab,
    selectedTab,
    loginData,
    setBankerEmail,
    modalOpen,
    setModalOpen,
    localMenu,
    globalMenu,
  } = useContext(GlobalContex);

  const capitalizedMenu = [
    {
      menuName: "Loans",
      menuIcon: loans,
      enabled: false,
    },
    {
      menuName: "CRM",
      menuIcon: crm,
      enabled: true,
    },
    {
      menuName: "Affilaites",
      menuIcon: affiliates,
      enabled: false,
    },
    {
      menuName: "Terminal",
      menuIcon: terminal,
      enabled: false,
    },
  ];

  return (
    <>
      <div className={collapse ? "grid-cap-collapsed" : "grid-cap"}>
        <GlobalSidebar globalMenu={globalMenu} />
        <LocalSidebar localMenu={capitalizedMenu} />
        <div
          style={{
            display: "flex",
            flexDirection: "column",
          }}
        >
          <TabsLayout />
          {/* <div className="filterRow"></div> */}
          {/* <div style={{ flexGrow: 1 }}>{conditionalPages()}</div> */}
        </div>
      </div>
      {modalOpen && (
        <AdminModal
          onClose={() => setModalOpen(false)}
          onSuccess={() => setModalOpen(false)}
        />
      )}
    </>
  );
};

export default Capitalized;
