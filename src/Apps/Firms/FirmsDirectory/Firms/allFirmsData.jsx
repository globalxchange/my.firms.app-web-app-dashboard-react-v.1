import React from "react";
import Skeleton from "react-loading-skeleton";

const AllFirmsData = ({ props }, i) => {
  console.log(props.businessCategory, "all firms data");
  return (
    <div>
      <div className="all-firms-data">
        {/* start */}
        <div className="firms-details long-width">
          {props ? (
            <img src={props.icon} alt="profile-img" className="the-firms-img" />
          ) : (
            <Skeleton className="the-firms-img" circle />
          )}

          <div className="firms-fields fields1">
            <p className="thefirmsname">
              {props ? props.business_name : <Skeleton width={150} />}
            </p>
            <p className="thefirmstag">
              {props ? props.business_code : <Skeleton width={150} />}
            </p>
          </div>
        </div>
        <div className="firms-details long-width">
          {props ? (
            <img
              src={props.businessCreator.profile_pic}
              alt="profile-img"
              className="the-firms-img"
            />
          ) : (
            <Skeleton className="the-firms-img" circle />
          )}

          <div className="firms-fields fields1">
            <p className="thefirmsname">
              {props ? props.businessCreator.email : <Skeleton width={150} />}
            </p>
            <p className="thefirmstag">
              {props ? (
                props.businessCreator.user_name
              ) : (
                <Skeleton width={150} />
              )}
            </p>
          </div>
        </div>
        <div className="firms-details long-width">
          {props ? (
            <img
              src={props.country_icon}
              alt="profile-img"
              className="the-firms-img"
            />
          ) : (
            <Skeleton className="the-firms-img" circle />
          )}

          <div className="firms-fields fields1">
            <p className="thefirmsname">
              {props ? props.country : <Skeleton width={150} />}
            </p>
            <p className="thefirmstag">
              {props ? props.address : <Skeleton width={150} />}
            </p>
          </div>
        </div>

        <div className="firms-details long-width">
          {props ? (
            <img
              src={
                props.businessCategory[0] ? props.businessCategory[0].icon : ""
              }
              alt="profile-img"
              className="the-firms-img"
            />
          ) : (
            <Skeleton className="the-firms-img" circle />
          )}

          <div className="firms-fields fields1">
            <p className="thefirmsname">
              {props.businessCategory[0] ? (
                props.businessCategory[0].category_name
              ) : (
                <Skeleton width={150} />
              )}
            </p>
            <p className="thefirmstag">
              {props ? props.business_type : <Skeleton width={150} />}
            </p>
          </div>
        </div>

        {/* end  && delete start*/}

        {/* delete end */}

        <div className="firms-data ">
          <p className="thefirmsname">
            {props ? props.totalMarketplace : <Skeleton width={150} />}
          </p>
        </div>
      </div>
      <hr className="hr-nav" />
    </div>
  );
};

export default AllFirmsData;
