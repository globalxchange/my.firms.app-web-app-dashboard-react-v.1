import React, { useContext, useEffect, useState } from "react";
import "./firms.scss";
import axios from "axios";
import { GlobalContex } from "../../../../globalContex";
import AllFirmsData from "./allFirmsData";
import FirmsSkeletonLoading from "./firmsLoadingSkeleton";

function Firms() {
  const { firmsDirectoryFirms, setFirmsDirectoryFirms } =
    useContext(GlobalContex);
  console.log("firms working");

  useEffect(() => {
    axios.get("https://commerce.apimachine.com/businesses").then((response) => {
      console.log(response.data.data.business_data, "firms response");
      setFirmsDirectoryFirms(response.data.data.business_data);
    });
  }, []);

  return (
    <div>
      <hr className="hr-nav" />
      <div className="firms-types">
        <p className="tag-type-fields long-width long-width-tag">Name</p>
        <p className="tag-type-fields">Owner</p>
        <p className="tag-type-fields">Address</p>
        <p className="tag-type-fields ">Industry</p>
        <p className="tag-type-fields ">Marketplaces</p>
      </div>
      <hr className="hr-nav" />
      <div className="the-firms-scroll">
        {firmsDirectoryFirms
          ? firmsDirectoryFirms.map((data) => {
              return <AllFirmsData props={data} />;
            })
          : [...Array(20)].map((data) => {
              return <FirmsSkeletonLoading />;
            })}
      </div>
    </div>
  );
}

export default Firms;
