import React from "react";
import Skeleton from "react-loading-skeleton";

const FirmsSkeletonLoading = () => {
  return (
    <div>
      <div className="all-firms-data">
        <div className="firms-details long-width">
          <Skeleton className="the-firms-img" circle />

          <div className="firms-fields fields1">
            <p className="thename">
              <Skeleton width={150} />
            </p>
            <p className="thefirmstag">
              <Skeleton width={150} />
            </p>
          </div>
        </div>
        <div className="firms-details long-width">
          <Skeleton className="the-firms-img" circle />

          <div className="firms-fields fields1">
            <p className="thename">
              <Skeleton width={150} />
            </p>
            <p className="thefirmstag">
              <Skeleton width={150} />
            </p>
          </div>
        </div>
        <div className="firms-details long-width">
          <Skeleton className="the-firms-img" circle />

          <div className="firms-fields fields1">
            <p className="thename">
              <Skeleton width={150} />
            </p>
            <p className="thefirmstag">
              <Skeleton width={150} />
            </p>
          </div>
        </div>
        <div className="firms-details long-width">
          <Skeleton className="the-firms-img" circle />

          <div className="firms-fields fields1">
            <p className="thename">
              <Skeleton width={150} />
            </p>
            <p className="thefirmstag">
              <Skeleton width={150} />
            </p>
          </div>
        </div>
        <div className="firms-data ">
          <p className="thename">
            <Skeleton width={50} />
          </p>
        </div>
      </div>
      <hr className="hr-nav" />
    </div>
  );
};

export default FirmsSkeletonLoading;
