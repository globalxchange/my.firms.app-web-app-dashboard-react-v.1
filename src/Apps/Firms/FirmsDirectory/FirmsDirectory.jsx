import React, { useEffect } from "react";
import { useState } from "react";
import { useContext } from "react";
import NavBar from "../../../globalComponents/NavBar";
import Users from "./Users/users";
import { GlobalContex } from "../../../globalContex";
import Firms from "./Firms/firms";

const FirmsDirectory = () => {
  const { setTabs } = useContext(GlobalContex);

  const tabs = ["Users", "Firms"];

  //   const { tabSelected, setTabSelected } = useContext(GlobalContex);

  const [tabSelected, setTabSelected] = useState("Users");

  function ConditionalTabs() {
    switch (tabSelected) {
      case "Users":
        return <Users tabSelected={tabSelected} />;
      case "Firms":
        return <Firms />;
    }
  }

  return (
    <div>
      <NavBar
        tabs={tabs}
        tabSelected={tabSelected}
        setTabSelected={setTabSelected}
        enabledFilters={[true, true, true, false, false]}
      />
      <div>{ConditionalTabs()}</div>
    </div>
  );
};

export default FirmsDirectory;
