import React from "react";
import Skeleton from "react-loading-skeleton";

const UsersSkeletonLoading = ({ firmsdirectoryuser }) => {
  return (
    <div>
      <div className="all-user-data">
        <div className="user-details long-width">
          <Skeleton className="the-user-img" circle />

          <div className="user-fields fields1">
            <p className="thename">
              <Skeleton width={150} />
            </p>
            <p className="thetag">
              <Skeleton width={150} />
            </p>
          </div>
        </div>
        <div className="user-data fields1">
          <p className="thename">
            <Skeleton width={150} />
          </p>
          <p className="thetag">
            <Skeleton width={150} />
          </p>
        </div>
        <div className="user-data ">
          <p className="thename">
            <Skeleton width={150} />
          </p>
          <p className="thetag">
            <Skeleton width={150} />
          </p>
        </div>
        <div className="user-data ">
          <p className="thename">
            <Skeleton width={150} />
          </p>
          <p className="thetag">
            <Skeleton width={150} />
          </p>
        </div>
        <div className="user-data ">
          <p className="thename">
            <Skeleton width={150} />
          </p>
          <p className="thetag">
            <Skeleton width={150} />
          </p>
        </div>
      </div>
      <hr className="hr-nav" />
    </div>
  );
};

export default UsersSkeletonLoading;
