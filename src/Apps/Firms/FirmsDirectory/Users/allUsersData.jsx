import React from "react";
import Skeleton from "react-loading-skeleton";

const AllUsersData = ({ firmsdirectoryuser }) => {
  return (
    <div>
      <div className="all-user-data">
        <div className="user-details long-width">
          {firmsdirectoryuser ? (
            <img
              src={firmsdirectoryuser.profile_pic}
              alt="profile-img"
              className="the-user-img"
            />
          ) : (
            <Skeleton className="the-user-img" circle />
          )}

          <div className="user-fields fields1">
            <p className="thename">
              {firmsdirectoryuser ? (
                firmsdirectoryuser.full_name
              ) : (
                <Skeleton width={150} />
              )}
            </p>
            <p className="thetag">
              {firmsdirectoryuser ? (
                firmsdirectoryuser.user_name
              ) : (
                <Skeleton width={150} />
              )}
            </p>
          </div>
        </div>
        <div className="user-data fields1">
          <p className="thename">
            {firmsdirectoryuser ? (
              firmsdirectoryuser.contact_no
            ) : (
              <Skeleton width={150} />
            )}
          </p>
          <p className="thetag">
            {firmsdirectoryuser ? (
              firmsdirectoryuser.email
            ) : (
              <Skeleton width={150} />
            )}
          </p>
        </div>
        <div className="user-data ">
          <p className="thename">
            {firmsdirectoryuser ? (
              firmsdirectoryuser.marketplace_count + " Joined"
            ) : (
              <Skeleton width={150} />
            )}
          </p>
          <p className="thetag">
            {firmsdirectoryuser ? (
              firmsdirectoryuser.marktetplaceowner_count + " Founded"
            ) : (
              <Skeleton width={150} />
            )}
          </p>
        </div>
        <div className="user-data ">
          <p className="thename">
            {firmsdirectoryuser ? (
              firmsdirectoryuser.Specialist_count + " Specalists"
            ) : (
              <Skeleton width={150} />
            )}
          </p>
          <p className="thetag">
            {firmsdirectoryuser ? (
              firmsdirectoryuser.businessowner_count + " Retailer"
            ) : (
              <Skeleton width={150} />
            )}
          </p>
        </div>
        <div className="user-data ">
          <p className="thename">
            {firmsdirectoryuser ? (
              "$" + firmsdirectoryuser.vaultData.total_vault_holdings.toFixed(2)
            ) : (
              <Skeleton width={150} />
            )}
          </p>
          <p className="thetag">
            {firmsdirectoryuser ? "$-.-- Spent" : <Skeleton width={150} />}
          </p>
        </div>
      </div>
      <hr className="hr-nav" />
    </div>
  );
};

export default AllUsersData;
