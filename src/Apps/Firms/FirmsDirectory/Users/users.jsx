import React, { useContext, useEffect } from "react";
import "./users.scss";
import axios from "axios";
import { GlobalContex } from "../../../../globalContex";
import AllUsersData from "./allUsersData";
import UsersSkeletonLoading from "./UsersSkeletonLoading";

function Users() {
  const { firmsDirectoryUser, setFirmsDirectoryUser } =
    useContext(GlobalContex);

  useEffect(() => {
    axios
      .get("https://commerce.apimachine.com/marketplaces/platform/users")
      .then((response) => {
        console.log("users", response.data.data.platformUser_data);
        setFirmsDirectoryUser(response.data.data.platformUser_data);
      });
  }, []);

  return (
    <div>
      <hr className="hr-nav" />
      <div className="user-types">
        <p className="type-fields long-width long-width-usersname">Name</p>
        <p className="type-fields">Contact</p>
        <p className="type-fields">Marketplaces</p>
        <p className="type-fields ">Profiles</p>
        <p className="type-fields ">Assets</p>
      </div>
      <hr className="hr-nav" />
      <div className="the-user-scroll">
        {firmsDirectoryUser
          ? firmsDirectoryUser.map((data) => {
              return <AllUsersData firmsdirectoryuser={data} />;
            })
          : [...Array(5)].map((data) => {
              return <UsersSkeletonLoading />;
            })}
      </div>
    </div>
  );
}

export default Users;
