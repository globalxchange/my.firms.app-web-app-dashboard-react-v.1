import React from "react";
import { useState } from "react";
import { useContext } from "react";
import NavBar from "../../globalComponents/NavBar";
import { GlobalContex } from "../../globalContex";

const LegalDashboard = () => {
  const tabs = ["Overview"];

  //   const { tabSelected, setTabSelected } = useContext(GlobalContex);

  const [tabSelected, setTabSelected] = useState("Overview");

  return (
    <div>
      <NavBar
        tabs={tabs}
        tabSelected={tabSelected}
        setTabSelected={setTabSelected}
        enabledFilters={[true, true, true, false, false]}
      />
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          fontWeight: 700,
          fontSize: "20px",
          height: "70vh",
        }}
      >
        This is InstaLegal Dashboard
      </div>
    </div>
  );
};

export default LegalDashboard;
