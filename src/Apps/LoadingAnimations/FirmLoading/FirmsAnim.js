import { memo } from "react";
import { motion } from "framer-motion";
import { useLottie } from "lottie-react";
import eventMapLottie from "../Lotties/eventMapLottie.json";
import classNames from "../Loading/loadings.scss"
import "./FirmsAnim.scss"

const icon = {
    hidden: {
        opacity: 0,
        pathLength: 0,
        fill: "#ffffff",
    },
    visible: {
        opacity: 1,
        pathLength: 1,
        fill: "#1F304F",
    },
};

function FirmsAnim({ className, classNameSvg }) {
    const options = {
        animationData: eventMapLottie,
        loop: true,
        autoplay: true,
        className: `${classNames.logo} ${classNameSvg}`,
    };
    const { View } = useLottie(options);
    return (
        <div className="loadinganim">
            <motion.svg
                className={`${classNames.logo} ${classNameSvg}`}
                viewBox="0 0 50 50"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
            >
                <motion.path
                    stroke="#1F304F"
                    strokeWidth={1}
                    variants={icon}
                    initial="hidden"
                    animate="visible"
                    transition={{
                        default: { duration: 2, ease: "easeInOut", repeat: Infinity },
                        fill: { duration: 2, ease: [1, 0, 0.8, 1], repeat: Infinity },
                    }}
                    d="M18.5432 3.93312V32.1797L14.3286 33.4503V5.19714L15.3036 4.90524L16.4751 4.5543L18.5432 3.93312Z" fill="#FFFFFF" />

                <motion.path d="M23.0383 5.88442V6.3145L20.2792 5.00791L19.3856 4.58372V4.15233L21.4666 5.13817L23.0383 5.88442Z" fill="#021842" />
                <motion.path d="M23.0383 7.28871V7.71895L20.2793 6.41211L19.3859 5.98834V5.55682L21.4666 6.5426L23.0383 7.28871Z" fill="#021842" />
                <motion.path d="M23.0379 8.69358V9.12381L20.2793 7.8172L19.3857 7.39335V6.96185L21.4661 7.94786L23.0379 8.69358Z" fill="#021842" />
                <motion.path d="M23.0379 10.0986V10.528L20.2796 9.22208L19.3855 8.79767V8.36634L21.4667 9.35223L23.0379 10.0986Z" fill="#021842" />
                <motion.path d="M23.0381 11.5034V11.9337L20.2791 10.6271L19.3855 10.2023V9.77133L21.4661 10.758L23.0381 11.5034Z" fill="#021842" />
                <motion.path d="M23.038 12.9081V13.338L20.2795 12.0318L19.3854 11.6074V11.1763L21.4664 12.1626L23.038 12.9081Z" fill="#021842" />
                <motion.path d="M23.0383 14.3129V14.7428L20.2794 13.4363L19.3859 13.0118V12.5808L21.4665 13.5671L23.0383 14.3129Z" fill="#021842" />
                <motion.path d="M23.0381 15.7175V16.1477L20.2795 14.8415L19.3857 14.4172V13.9857L21.4663 14.9716L23.0381 15.7175Z" fill="#021842" />
                <motion.path d="M23.0381 17.1221V17.5519L20.2791 16.2459L19.3859 15.8213V15.3903L21.4662 16.3765L23.0381 17.1221Z" fill="#021842" />
                <motion.path d="M23.0382 18.5275V18.9568L20.2793 17.651L19.3855 17.2263V16.7952L21.4663 17.7814L23.0382 18.5275Z" fill="#021842" />
                <motion.path d="M23.038 19.9318V20.3615L20.2794 19.0552L19.3859 18.6312V18.1998L21.4663 19.186L23.038 19.9318Z" fill="#021842" />
                <motion.path d="M23.0378 21.3368V21.7667L20.2791 20.4601L19.3856 20.0358V19.6047L21.4664 20.5911L23.0378 21.3368Z" fill="#021842" />
                <motion.path d="M23.0382 22.7415V23.1714L20.2795 21.8651L19.3854 21.4407V21.0097L21.4662 21.9958L23.0382 22.7415Z" fill="#021842" />
                <motion.path d="M23.0383 24.1461V24.5758L20.2793 23.2698L19.3857 22.8455V22.4142L21.4664 23.4001L23.0383 24.1461Z" fill="#021842" />
                <motion.path d="M23.038 25.5513V25.9811L20.2791 24.6745L19.3858 24.2506V23.8192L21.4661 24.8054L23.038 25.5513Z" fill="#021842" />
                <motion.path d="M23.0381 26.9557V27.3854L20.2795 26.0789L19.3859 25.655V25.2237L21.4663 26.2097L23.0381 26.9557Z" fill="#021842" />
                <motion.path d="M23.0378 28.3607V28.7906L20.2791 27.4844L19.3857 27.0601V26.6287L21.466 27.6146L23.0378 28.3607Z" fill="#021842" />
                <motion.path d="M23.0382 29.7653V30.1951L20.2792 28.8888L19.3855 28.4645V28.0332L21.4662 29.0194L23.0382 29.7653Z" fill="#021842" />
                <motion.path d="M22.5941 30.9596L22.0392 31.1269L20.279 30.2938L19.3854 29.8692V29.4382L21.4663 30.4246L22.5941 30.9596Z" fill="#021842" />
                <motion.path d="M20.7831 31.5063L20.2255 31.6732L19.3856 31.2746V30.8431L20.7831 31.5063Z" fill="#021842" />
                <motion.path stroke="#1F304F"
                    strokeWidth={1}
                    variants={icon}
                    initial="hidden"
                    animate="visible"
                    transition={{
                        default: { duration: 2, ease: "easeInOut", repeat: Infinity },
                        fill: { duration: 2, ease: [1, 0, 0.8, 1], repeat: Infinity },
                    }} d="M26.7903 10.3546V29.6954L23.4597 30.6997V11.3534L24.2291 11.1228L25.1564 10.8447L26.7903 10.3546Z" fill="#021842" />
                <motion.path d="M30.3429 11.8965V12.2363L28.1628 11.2041L27.4562 10.8687V10.5277L29.1002 11.3069L30.3429 11.8965Z" fill="#021842" />
                <motion.path d="M30.3431 13.0066V13.3462L28.1628 12.3139L27.4567 11.9785V11.6376L29.1008 12.4167L30.3431 13.0066Z" fill="#021842" />
                <motion.path d="M30.3429 14.1166V14.4566L28.1629 13.4238L27.4563 13.0886V12.7479L29.1006 13.5274L30.3429 14.1166Z" fill="#021842" />
                <motion.path d="M30.3431 15.2269V15.5667L28.1628 14.5343L27.4564 14.1993V13.8583L29.1006 14.6378L30.3431 15.2269Z" fill="#021842" />
                <motion.path d="M30.3433 16.3371V16.6765L28.1626 15.6441L27.4567 15.3088V14.9682L29.1009 15.7477L30.3433 16.3371Z" fill="#021842" />
                <motion.path d="M30.3428 17.4473V17.7869L28.1624 16.7548L27.4565 16.4193V16.0786L29.1008 16.8577L30.3428 17.4473Z" fill="#021842" />
                <motion.path d="M30.3428 18.5577V18.8973L28.1627 17.8653L27.4565 17.5297V17.1889L29.1005 17.9681L30.3428 18.5577Z" fill="#021842" />
                <motion.path d="M30.3432 19.6679V20.0074L28.1627 18.9751L27.4564 18.6396V18.2989L29.1008 19.0779L30.3432 19.6679Z" fill="#021842" />
                <motion.path d="M30.3432 20.7781V21.1175L28.1626 20.0852L27.4563 19.7497V19.4092L29.1006 20.1885L30.3432 20.7781Z" fill="#021842" />
                <motion.path d="M30.3427 21.8885V22.2282L28.1625 21.1957L27.4565 20.8604V20.5196L29.1002 21.2988L30.3427 21.8885Z" fill="#021842" />
                <motion.path d="M30.3433 22.9985V23.3379L28.1625 22.3055L27.4564 21.97V21.6295L29.1008 22.409L30.3433 22.9985Z" fill="#021842" />
                <motion.path d="M30.3428 24.1086V24.4481L28.1627 23.4161L27.4567 23.0804V22.7399L29.1006 23.5194L30.3428 24.1086Z" fill="#021842" />
                <motion.path d="M30.3429 25.2194V25.5588L28.1625 24.5265L27.4563 24.1908V23.8503L29.1003 24.6295L30.3429 25.2194Z" fill="#021842" />
                <motion.path d="M30.3429 26.3292V26.6685L28.1629 25.6362L27.4568 25.3011V24.9601L29.1009 25.7395L30.3429 26.3292Z" fill="#021842" />
                <motion.path d="M30.3433 27.4395V27.7792L28.1628 26.7467L27.4565 26.4112V26.0705L29.1006 26.8497L30.3433 27.4395Z" fill="#021842" />
                <motion.path d="M30.3432 28.55V28.6244L30.0034 28.7271H30.0018L28.1626 27.8565L27.4562 27.5218V27.1804L29.101 27.9602L30.3432 28.55Z" fill="#021842" />
                <motion.path d="M29.0096 29.0263L28.5689 29.16L28.1627 28.9676L27.4561 28.6317V28.2903L29.0096 29.0263Z" fill="#021842" />
                <motion.path stroke="#1F304F"
                    strokeWidth={1}
                    variants={icon}
                    initial="hidden"
                    animate="visible"
                    transition={{
                        default: { duration: 2, ease: "easeInOut", repeat: Infinity },
                        fill: { duration: 2, ease: [1, 0, 0.8, 1], repeat: Infinity },
                    }}  d="M33.429 18.2578V27.6951L31.1857 28.3706V18.9304L31.7044 18.7747L32.3276 18.5878L33.429 18.2578Z" fill="#FFFFFF" />
                <motion.path d="M35.8218 19.2958V19.5244L34.3535 18.8294L33.8774 18.6036V18.3738L34.9849 18.8986L35.8218 19.2958Z" fill="#021842" />
                <motion.path d="M35.8217 20.0433V20.2724L34.3534 19.5768L33.8779 19.3511V19.1215L34.9848 19.6466L35.8217 20.0433Z" fill="#021842" />
                <motion.path d="M35.8217 20.7913V21.0202L34.3531 20.3247L33.8775 20.0992V19.8692L34.9849 20.3944L35.8217 20.7913Z" fill="#021842" />
                <motion.path d="M35.8216 21.5386V21.7672L34.3534 21.0725L33.8779 20.8466V20.6169L34.9851 21.1417L35.8216 21.5386Z" fill="#021842" />
                <motion.path d="M35.8215 22.2864V22.5153L34.3529 21.8197L33.8774 21.5941V21.3646L34.9848 21.8894L35.8215 22.2864Z" fill="#021842" />
                <motion.path d="M35.8213 23.034V23.2632L34.353 22.5674L33.8777 22.342V22.1123L34.9846 22.6371L35.8213 23.034Z" fill="#021842" />
                <motion.path d="M35.8218 23.782V24.0104L34.3535 23.3156L33.8779 23.0894V22.86L34.9851 23.3847L35.8218 23.782Z" fill="#021842" />
                <motion.path d="M35.8215 24.5294V24.7585L34.3533 24.0632L33.8774 23.8373V23.6077L34.9849 24.1328L35.8215 24.5294Z" fill="#021842" />
                <motion.path d="M35.8212 25.2772V25.5064L34.3529 24.8107L33.8774 24.5846V24.3554L34.9849 24.8802L35.8212 25.2772Z" fill="#021842" />
                <motion.path d="M35.8211 26.0248V26.2542L34.3528 25.5587L33.8773 25.333V25.1031L34.9846 25.6282L35.8211 26.0248Z" fill="#021842" />
                <motion.path d="M35.8214 26.7729V27.0023L35.7849 26.9851L34.3534 26.3067L33.8773 26.0809V25.8504L34.9844 26.376L35.8214 26.7729Z" fill="#021842" />
                <motion.path d="M35.1179 27.1862L34.8212 27.2761L34.3534 27.0538L33.8774 26.828V26.599L34.9841 27.1231L35.1179 27.1862Z" fill="#021842" />
                <motion.path d="M35.8218 26.974V27.0023L35.7852 26.9853L35.8218 26.974Z" fill="#021842" />
                <motion.path stroke="#FFFFFF"
                    strokeWidth={1}
                    variants={icon}
                    initial="hidden"
                    animate="visible"
                    transition={{
                        default: { duration: 2, ease: "easeInOut", repeat: Infinity },
                        fill: { duration: 2, ease: [1, 0, 0.8, 1], repeat: Infinity },
                    }} d="M44.0564 25.0547C44.0451 25.1422 44.0338 25.228 44.0197 25.3149L42.1699 26.1309L35.8218 28.9283L35.6784 28.9909L35.1113 29.2415L34.8607 29.351L34.2947 29.601L34.0436 29.7121L33.4296 29.9813L31.1858 30.9706L30.3433 31.3425L29.9838 31.5016L29.1424 31.8724L28.7699 32.0355H28.7682L27.9285 32.4062L27.5549 32.5704L26.7908 32.9079L23.4602 34.3757L11.563 39.6183L9.71428 40.4326C8.14494 39.3655 6.72063 38.1025 5.47449 36.6793L7.26143 36.1415L14.3288 34.0117L18.5432 32.742L20.226 32.2347L20.7836 32.0676L22.0397 31.6884L22.5945 31.5214L23.4602 31.2612L26.7908 30.2567L27.4562 30.0558L27.5769 30.0208L28.569 29.7217L29.0098 29.5885L30.0018 29.2889H30.0035L30.3433 29.1862L31.1858 28.9322L33.4296 28.2562L33.8776 28.1218L34.153 28.0389L34.8218 27.838L35.118 27.7483L35.7851 27.5474L35.8218 27.5637V27.5361L42.2698 25.5931L44.0564 25.0547Z" fill="#021842" />
                <motion.path stroke="#FFFFFF"
                    strokeWidth={1}
                    variants={icon}
                    initial="hidden"
                    animate="visible"
                    transition={{
                        default: { duration: 2, ease: "easeInOut", repeat: Infinity },
                        fill: { duration: 2, ease: [1, 0, 0.8, 1], repeat: Infinity },
                    }} d="M44.0043 25.4231C43.9891 25.5314 43.9716 25.638 43.9535 25.7451L41.9801 26.991L17.9686 42.1386L15.9917 43.3862C14.1096 42.8425 12.3296 42.054 10.6889 41.0596L12.5586 40.1832L42.1357 26.3012L44.0043 25.4231Z" fill="#021842" />
                <motion.path stroke="#FFFFFF"
                    strokeWidth={1}
                    variants={icon}
                    initial="hidden"
                    animate="visible"
                    transition={{
                        default: { duration: 2, ease: "easeInOut", repeat: Infinity },
                        fill: { duration: 2, ease: [1, 0, 0.8, 1], repeat: Infinity },
                    }} d="M43.9381 25.8419C43.9127 25.9879 43.8879 26.1328 43.8592 26.2776L41.6341 28.226L25.5917 42.2692L23.3695 44.2148C22.958 44.239 22.5421 44.2497 22.125 44.2497C20.5317 44.2497 18.9784 44.0812 17.481 43.7594L19.4936 42.396L41.9249 27.2047L43.9381 25.8419Z" fill="#021842" />
                <motion.path stroke="#1F304F"
                    strokeWidth={1}
                    variants={icon}
                    initial="hidden"
                    animate="visible"
                    transition={{
                        default: { duration: 2, ease: "easeInOut", repeat: Infinity },
                        fill: { duration: 2, ease: [1, 0, 0.8, 1], repeat: Infinity },
                    }} d="M41.9799 26.9911C41.9615 27.0625 41.9432 27.1332 41.9248 27.2046L43.9378 25.8417C43.944 25.8098 43.948 25.7771 43.9535 25.7452L41.9799 26.9911ZM42.1696 26.1315C42.1581 26.1872 42.1472 26.245 42.1356 26.3015C42.0867 26.5334 42.0343 26.7619 41.9799 26.9911C41.9615 27.0625 41.9432 27.1332 41.9248 27.2046C41.8364 27.5488 41.7398 27.8888 41.6344 28.2261C39.3697 35.4533 33.1898 40.9668 25.592 42.2691C24.4651 42.4629 23.3062 42.5643 22.1249 42.5643C21.2347 42.5643 20.3546 42.5065 19.4937 42.3956C18.9798 42.3297 18.4698 42.244 17.9682 42.1385C16.0535 41.7421 14.2372 41.0763 12.5587 40.1833C12.2213 40.0044 11.8884 39.8161 11.5625 39.6188C9.9751 38.6565 8.52836 37.4847 7.26123 36.1416C3.80696 32.4793 1.68572 27.546 1.68572 22.1251C1.68572 10.8548 10.8546 1.68592 22.1249 1.68592C33.3952 1.68592 42.5641 10.8548 42.5641 22.1251C42.5641 23.3078 42.4627 24.4666 42.2689 25.5935C42.2383 25.7744 42.2064 25.9533 42.1696 26.1315ZM22.1249 0C9.92464 0 0 9.92443 0 22.1251C0 27.6922 2.0663 32.7874 5.47425 36.6795C6.72037 38.1029 8.14473 39.3659 9.71395 40.4329C10.0328 40.6512 10.3587 40.8586 10.6888 41.0599C12.3295 42.0542 14.1094 42.8424 15.9919 43.3861C16.482 43.5265 16.978 43.6515 17.481 43.7596C18.9784 44.0813 20.5321 44.25 22.1249 44.25C22.5425 44.25 22.958 44.2388 23.3694 44.215C33.5931 43.6458 41.9853 36.1035 43.8596 26.2777C43.8875 26.1328 43.9127 25.988 43.9378 25.8417C43.944 25.8098 43.948 25.7771 43.9535 25.7452C43.9718 25.6384 43.9888 25.5316 44.0045 25.4235C44.0099 25.3868 44.0154 25.3514 44.0194 25.3154C44.0337 25.2283 44.0446 25.1426 44.0562 25.0549C44.184 24.096 44.25 23.118 44.25 22.1251C44.25 9.92443 34.3256 0 22.1249 0ZM41.9799 26.9911C41.9615 27.0625 41.9432 27.1332 41.9248 27.2046L43.9378 25.8417C43.944 25.8098 43.948 25.7771 43.9535 25.7452L41.9799 26.9911Z" fill="#FFFFFF" />

                {/* <motion.motion.path
          stroke="#1F304F"
          strokeWidth={1}
          variants={icon}
          initial="hidden"
          animate="visible"
          transition={{
            default: { duration: 2, ease: "easeInOut", repeat: Infinity },
            fill: { duration: 2, ease: [1, 0, 0.8, 1], repeat: Infinity },
          }}
          d="M17.9061 21.694C20.9302 22.805 23.9346 22.3777 26.9653 21.6677C32.4614 20.3857 37.9182 20.8919 43.3419 22.1673C43.9991 22.3251 44.2687 22.713 44.249 23.3572C44.2292 23.9095 44.1768 24.4683 44.1768 25.0205C44.1768 25.4742 44.1305 25.816 43.5325 25.7634C42.9998 25.7174 42.9933 26.1513 42.9409 26.5129C42.7697 27.7554 42.704 29.0177 42.3755 30.2405C41.4418 33.6788 38.7528 35.9139 35.2159 36.2165C33.5856 36.3543 31.9684 36.3809 30.3379 36.1903C28.6747 35.9929 27.3006 35.3158 26.2553 33.9944C24.901 32.2785 23.9609 30.3785 23.79 28.163C23.6848 26.7824 23.2838 26.3419 22.0939 26.3617C20.8579 26.3814 20.4306 26.8613 20.3451 28.2813C20.2005 30.7007 19.2209 32.6992 17.5839 34.5135C14.6715 37.7349 4.16593 37.2159 2.14763 31.2332C1.65456 29.7342 1.35215 28.2222 1.41789 26.6509C1.43104 26.2368 1.48363 25.7634 0.872229 25.7963C0.0635999 25.8357 0.0175802 25.3492 0.00443176 24.7378C-0.0350137 22.4631 0.14249 22.2068 2.46977 21.7794C7.53193 20.8459 12.6072 20.6158 17.6891 21.6742C17.768 21.5953 17.8403 21.6085 17.9061 21.694Z" fill="white" />
            <motion.motion.path
          stroke="#1F304F"
          strokeWidth={1}
          variants={icon}
          initial="hidden"
          animate="visible"
          transition={{
            default: { duration: 2, ease: "easeInOut", repeat: Infinity },
            fill: { duration: 2, ease: [1, 0, 0.8, 1], repeat: Infinity },
          }}
          d="M30.0552 2.88507C26.5051 1.04428 23.0997 1.62282 19.714 2.74701C17.4984 3.4899 15.2764 4.14075 12.8965 3.75945C11.8775 3.59508 10.9308 3.2598 9.89862 2.32626C14.5335 3.26637 18.3728 1.15604 22.4357 0.321116C24.0793 -0.0141696 25.7294 -0.13908 27.3598 0.419729C28.5497 0.820757 29.4635 1.53735 30.0552 2.88507Z" fill="#FDFEFD"/> */}
            </motion.svg>
        </div>
    );
}

export default memo(FirmsAnim);