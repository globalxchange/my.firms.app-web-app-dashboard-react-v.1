import React from "react";
import { useContext } from "react";
import GlobalSidebar from "../../globalComponents/GlobalSidebar";
import LocalSidebar from "../../globalComponents/LocalSidebar";
import { GlobalContex } from "../../globalContex";

import TabsLayout from "../../globalComponents/TabsLayout";
import AdminModal from "../../globalComponents/AdminModal";

import crm from "../../static/images/sidebarIcons/crm.svg";
import dash from "../../static/images/sidebarIcons/dash.svg";
import affiliates from "../../static/images/sidebarIcons/affiliates.svg";
import vaults from "../../static/images/sidebarIcons/vaults.svg";
import terminal from "../../static/images/sidebarIcons/terminal.svg";
import bonds from "../../static/images/sidebarIcons/bonds.svg";
import loans from "../../static/images/sidebarIcons/socially.svg";
import Lock from "../../static/images/icons/lock.svg";
import defaultImg from "../../static/images/icons/app_placeholder.png";
import TaxChainsDashboard from "./TaxChainsDashboard";
import VaultPage from "../../globalComponents/VaultPage";
import Images from "../../static/images/theimages/0-exporter"
import DashIcon from "../../static/images/theimages/dashicon.svg"
import WalletIcon from "../../static/images/theimages/walletico.svg"
import CalculatorIco from "../../static/images/theimages/calcicon.svg"
import TaxFundIco from "../../static/images/theimages/taxfundico.svg"
import VauldIco from "../../static/images/theimages/vauldico.svg"
import ReceiptIco from "../../static/images/theimages/receiptico.svg"
import TaxedIco from "../../static/images/theimages/taxedico.svg"

const TaxChains = () => {
  const {
    collapse,
    setTabs,
    setSelectedTab,
    selectedTab,
    loginData,
    setBankerEmail,
    modalOpen,
    setModalOpen,
    localMenu,
    globalMenu,
  } = useContext(GlobalContex);

  const bankerMenu = [
    {
      menuName: "Dashboard",
      menuIcon: dash,
      enabled: true,
    },
    {
      menuName: "WalletScan",
      menuIcon: WalletIcon,
      enabled: true,
    },
    {
      menuName: "Calculator",
      menuIcon: CalculatorIco,
      enabled: true,
    },
    {
      menuName: "TaxFund",
      menuIcon: TaxFundIco,
      enabled: true,
    },
    {
      menuName: "Vauld",
      menuIcon: VauldIco,
      enabled: true,
    },
    {
      menuName: "Receipt",
      menuIcon: ReceiptIco,
      enabled: true,
    },
    {
      menuName: "Taxed",
      menuIcon: TaxedIco,
      enabled: true,
    },
  ];

  const conditionalPages = () => {
    console.log(selectedTab?.menuName, "khedhwedk");

    switch (selectedTab?.menuName) {
      case "Dashboard":
        return <TaxChainsDashboard />;
      case "Vaults":
        return <VaultPage />;
      default:
        return (
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              fontWeight: 700,
              fontSize: "20px",
              height: "70vh",
            }}
          >
            Coming Soon
          </div>
        );
        break;
    }
  };

  return (
    <>
      <div className={collapse ? "grid-cap-collapsed" : "grid-cap"}>
        <GlobalSidebar globalMenu={globalMenu} />
        <LocalSidebar localMenu={bankerMenu} />
        <div
          style={{
            display: "flex",
            flexDirection: "column",
          }}
        >
          <TabsLayout />
          <div style={{ flexGrow: 1 }}>{conditionalPages()}</div>
        </div>
      </div>
      {modalOpen && (
        <AdminModal
          onClose={() => setModalOpen(false)}
          onSuccess={() => setModalOpen(false)}
        />
      )}
    </>
  );
};

export default TaxChains;
