import React, { useEffect, useMemo, useState } from "react";

import OtpInput from "react-otp-input";
import classNames from "./settingsModal.module.scss";
import arrowGo from "../../static/images/clipIcons/arrowGo.svg";

import { toast } from "react-toastify";
import axios from "axios";

import { useContext } from "react";
import { GlobalContex } from "../../globalContex";
import { useRef } from "react";

import "./adminModal.scss";

function AdminModal({ onClose = () => {}, onSuccess = () => {}, logoParam }) {
  const {
    bankerEmail,
    loginData,
    setBankerEmail,
    selectedApp,
    loading,
    setLoading,
    query,
    setQuery,
    shareToken,
    setShareToken,
    banker,
    setBanker,
    allShareToken,
    setAllShareToken,
    allBankers,
    setAllBankers,
    setModalOpen,
  } = useContext(GlobalContex);

  const bankerEmailRef = useRef();

  const [step, setStep] = useState("");
  const [pin, setPin] = useState("");

  // const [query, setQuery] = useState("");
  const [newBankerData, setNewBankerData] = useState("");
  const [modalLoading, setModalLoading] = useState(false);

  // useEffect(() => {
  //   setLoading(true);
  //   axios
  //     .get(
  //       `https://comms.globalxchange.com/coin/investment/path/get?investmentType=EQT&banker_email=${localStorage.getItem(
  //         "bankerEmailNew"
  //       )}`
  //     )
  //     .then((res) => {
  //       if (res.data.status) {
  //         setLoading(false);
  //         setAllShareToken(res.data.paths);
  //         console.log(res.data.paths, "jhvdjhwvedhjwed");
  //       } else {
  //         setAllShareToken([]);
  //         setLoading(false);
  //       }
  //     });
  // }, [localStorage.getItem("bankerEmailNew")]);

  // useEffect(() => {
  //   setAllShareToken([]);
  //   axios.get(`https://teller2.apimachine.com/admin/allBankers`).then((res) => {
  //     setAllBankers(res.data.data);
  //   });
  // }, []);

  const filteredSharedToken = allShareToken
    ? allShareToken.filter((item) => {
        const lowquery = query.toLowerCase();
        return (
          (item.token_profile_data.coinSymbol + item.apps_metadata[0].app_name)
            .toLowerCase()
            .indexOf(lowquery) >= 0
        );
      })
    : "";

  const checkPinValidity = () => {
    axios
      .post(
        `https://comms.globalxchange.com/gxb/apps/user/validate/group/pin`,
        {
          email: loginData.user.email,
          token: loginData?.idToken,
          pin: pin,
          group_id: "66me7fdkhxsbtur",
        }
      )
      .then((res) => {
        if (res.data.status) {
          setModalLoading(false);
          setStep("Validated");
          setTimeout(() => {
            setStep("EnterEmail");
          }, 2000);
        } else {
          if (res.data.message === "jwt expired") {
            setModalLoading(false);
            setStep("InvalidJwt");
          } else {
            setModalLoading(false);
            setStep("Invalid");
          }
        }
      });
  };

  useEffect(() => {
    if (pin.length === 4) {
      setModalLoading(true);
      checkPinValidity();
    }
  }, [pin]);

  const handleSetBankerEmail = () => {
    axios
      .get(
        `https://comms.globalxchange.com/user/profile/data/get?email=${bankerEmailRef.current.value}`
      )
      .then((res) => {
        if (res.data.status) {
          console.log(res.data?.usersData, "kjwkdjwe");
          setNewBankerData(res.data.usersData[0]);
          setStep("VerifyUser");
          localStorage.setItem(
            "newBankerData",
            JSON.stringify(res.data.usersData[0])
          );
        }
      });

    // setBankerEmail(bankerEmailRef.current.value);
    // setStep(null);
  };

  const stepRender = useMemo(() => {
    switch (step) {
      case "Admin":
        return (
          <>
            <div className="modalStyle">
              <div className="headerSection">Settings</div>
              <div className="breadcrumb">
                <span>Settings</span>
                &nbsp;
                {"> "}
                <span style={{ textDecoration: "underline", fontWeight: 700 }}>
                  Admin
                </span>
              </div>
              <div className={classNames.inCard}>
                <div className={classNames.otpView}>
                  <div className={classNames.label}>Enter Admin PIN</div>
                  <OtpInput
                    value={pin}
                    onChange={setPin}
                    numInputs={4}
                    separator={<span> </span>}
                    shouldAutoFocus
                    containerStyle={classNames.otpInputWrapper}
                    inputStyle={classNames.otpInput}
                  />
                </div>
              </div>
            </div>

            <br />

            <div className={classNames.footerBtns} style={{ display: "flex" }}>
              <div
                onClick={(e) => setStep(null)}
                className={classNames.btnSettings}
                style={{ background: "#E7E7E780", color: "#292929" }}
              >
                <span>Back</span>
              </div>
              <div
                // onClick={(e) => checkPinValidity()}
                className={classNames.btnSettings}
                style={{
                  background: selectedApp.appColor,
                  borderLeft: "1px solid gray",
                }}
              >
                <span style={{ opacity: "0.5" }}>Next</span>
              </div>
            </div>
          </>
        );
      case "Validated":
        return (
          <>
            <div
              className="modalStyle"
              style={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                fontSize: "25px",
                fontWeight: 700,
                textAlign: "center",
              }}
            >
              Your Pin Has Been Successfully Validated
            </div>
          </>
        );
      case "EnterEmail":
        return (
          <>
            <div className="modalStyle">
              <div className="headerSection">Settings</div>
              <div className="breadcrumb">
                <span>Settings</span>
                &nbsp;
                {"> "}Admin{"> "}
                <span style={{ fontWeight: 700, textDecoration: "underline" }}>
                  Enter Email
                </span>
              </div>
              <br />
              <div className="titleSection">Enter Email</div>
              <div style={{ width: "100%" }}>
                <input
                  ref={bankerEmailRef}
                  placeholder="Enter Email"
                  // value={newBankerEmail}
                  // onChange={(e) => setNewBankerEmail(e.target.value)}
                  type="text"
                  style={{
                    borderRadius: "15px",
                    width: "100%",
                    border: "0.5px solid #E5E5E5",
                    height: "59px",
                    padding: "0px 20px",
                    color: "black",
                  }}
                />
              </div>
            </div>
            <br />

            <div className={classNames.footerBtns} style={{ display: "flex" }}>
              <div
                onClick={(e) => setStep(null)}
                className={classNames.btnSettings}
                style={{ background: "#E7E7E780", color: "#292929" }}
              >
                <span>Back</span>
              </div>
              <div
                onClick={(e) => handleSetBankerEmail()}
                className={classNames.btnSettings}
                style={{
                  background: selectedApp.appColor,
                  borderLeft: "1px solid gray",
                }}
              >
                <span>Next</span>
              </div>
            </div>
          </>
        );
      case "VerifyUser":
        return (
          <>
            <div className="modalStyle">
              <div className="headerSection">Settings</div>
              <div className="breadcrumb">
                Admin{"> "}Enter Email{"> "}
                <span style={{ fontWeight: 700, textDecoration: "underline" }}>
                  Verify User
                </span>
              </div>
              <br />
              <div className="titleSection">Enter Email</div>
              <div
                onClick={(e) => {
                  setBankerEmail(newBankerData?.hardCoded[0]?.data?.email);
                  setStep(null);
                }}
                style={{
                  width: "100%",
                  borderRadius: "15px",
                  width: "100%",
                  border: "0.5px solid #E5E5E5",
                  height: "65px",
                  padding: "0px 20px",
                  color: "black",
                  display: "flex",
                  alignItems: "center",
                  cursor: "pointer",
                }}
              >
                <img
                  src={newBankerData?.dynamic[0]?.data?.profile_img}
                  alt=""
                  style={{
                    width: "40px",
                    height: "40px",
                    borderRadius: "100%",
                  }}
                />
                <div style={{ paddingLeft: "10px" }}>
                  <div style={{ fontWeight: 700 }}>
                    {newBankerData?.hardCoded[0]?.data?.username}
                  </div>
                  <div>{newBankerData?.hardCoded[0]?.data?.email}</div>
                </div>
              </div>
            </div>
            <br />

            <div className={classNames.footerBtns} style={{ display: "flex" }}>
              <div
                onClick={(e) => setStep(null)}
                className={classNames.btnSettings}
                style={{ background: "#E7E7E780", color: "#292929" }}
              >
                <span>Back</span>
              </div>
              <div
                onClick={(e) => handleSetBankerEmail()}
                className={classNames.btnSettings}
                style={{
                  background: selectedApp.appColor,
                  borderLeft: "1px solid gray",
                }}
              >
                <span>Next</span>
              </div>
            </div>
          </>
        );

      case "Invalid":
        return (
          <>
            <div
              className="modalStyle"
              style={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                fontSize: "25px",
                fontWeight: 700,
                textAlign: "center",
              }}
            >
              The Pin You Entered Is Invalid or You Are Not An Admin.
            </div>
            <div className={classNames.footerBtns} style={{ display: "flex" }}>
              <div
                onClick={(e) => onClose()}
                className={classNames.btnSettings}
                style={{ background: "#E7E7E780", color: "#292929" }}
              >
                <span>Close</span>
              </div>
              <div
                onClick={(e) => setStep("Admin")}
                className={classNames.btnSettings}
                style={{
                  background: selectedApp.appColor,
                  borderLeft: "1px solid gray",
                }}
              >
                <span>Back</span>
              </div>
            </div>
          </>
        );
      case "InvalidJwt":
        return (
          <>
            <div
              className="modalStyle"
              style={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                fontSize: "25px",
                fontWeight: 700,
                textAlign: "center",
              }}
            >
              Token Has Expired.
              <br />
              Please Relogin.
            </div>
            <div className={classNames.footerBtns} style={{ display: "flex" }}>
              <div
                onClick={(e) => {
                  localStorage.clear();
                  window.location.reload();
                }}
                className={classNames.btnSettings1}
                style={{
                  background: selectedApp.appColor,
                  borderLeft: "1px solid gray",
                }}
              >
                <span>Sign-Out</span>
              </div>
            </div>
          </>
        );

      default:
        return (
          <>
            <div className="modalStyle">
              <div className="headerSection">Settings</div>
              <div className="breadcrumb">
                <span style={{ textDecoration: "underline", fontWeight: 700 }}>
                  Settings
                </span>
                &nbsp;
                {">"}
              </div>
              <div className="titleSection">What Do You Want To Change?</div>

              <div onClick={(e) => setStep("Admin")} className="menuCards1">
                <div>
                  <div style={{ fontSize: "16px", fontWeight: 700 }}>
                    Logged in User
                  </div>
                  <div
                    style={{
                      display: "flex",
                      alignItems: "center",
                    }}
                  >
                    <div style={{ fontSize: "12px", fontWeight: 400 }}>
                      Currently Selected:&nbsp;
                    </div>
                    <div>
                      {localStorage.getItem("newBankerData") ? (
                        <img
                          src={
                            JSON.parse(localStorage.getItem("newBankerData"))
                              .dynamic[0]?.data?.profile_img
                          }
                          alt=""
                          style={{
                            width: "17px",
                            height: "17px",
                            borderRadius: "100%",
                          }}
                        />
                      ) : loginData?.user?.profile_img ? (
                        <img
                          src={loginData?.user?.profile_img}
                          alt=""
                          style={{
                            width: "17px",
                            height: "17px",
                            borderRadius: "100%",
                          }}
                        />
                      ) : (
                        <div
                          style={{
                            height: "17px",
                            width: "17px",
                            borderRadius: "100%",
                          }}
                        >
                          &nbsp;
                        </div>
                      )}
                    </div>
                    <div style={{ fontSize: "12px", fontWeight: 700 }}>
                      &nbsp;{bankerEmail}
                    </div>
                  </div>
                </div>
              </div>

              <div className="menuCards" style={{ marginTop: "30px" }}>
                <div>
                  <div style={{ fontSize: "16px", fontWeight: 700 }}>
                    Application
                  </div>
                  <div style={{ display: "flex", alignItems: "center" }}>
                    <div style={{ fontSize: "12px", fontWeight: 400 }}>
                      Currently Selected:&nbsp;
                    </div>
                    <div
                      style={{
                        height: "17px",
                        width: "17px",
                        background: "gray",
                        borderRadius: "100%",
                      }}
                    >
                      &nbsp;
                    </div>
                    <div style={{ fontSize: "12px", fontWeight: 700 }}>
                      &nbsp;CCSWealth
                    </div>
                  </div>
                </div>
                <div>
                  <img src={arrowGo} alt="" width="20px" />
                </div>
              </div>
            </div>
            <div className={classNames.footerBtns} style={{ display: "flex" }}>
              <div
                onClick={(e) => onClose()}
                className={classNames.btnSettings}
                style={{ background: "#E7E7E780", color: "#292929" }}
              >
                <span>Close</span>
              </div>
              <div
                className={classNames.btnSettings}
                style={{
                  background: selectedApp.appColor,
                  borderLeft: "1px solid gray",
                }}
              >
                <span style={{ opacity: 0.4 }}>Next</span>
              </div>
            </div>
          </>
        );
    }
  }, [
    loginData.user.email,
    logoParam,
    onClose,
    onSuccess,
    pin,
    step,
    bankerEmail,
  ]);

  return (
    <>
      <div className={classNames.settingsModal}>
        <div
          className={classNames.overlayClose}
          onClick={() => {
            try {
              onClose();
            } catch (error) {}
          }}
        />
        <div className={classNames.settingsCard}>{stepRender}</div>
      </div>
    </>
  );
}

export default AdminModal;
