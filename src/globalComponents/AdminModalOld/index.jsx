import React, { useEffect, useMemo, useState, useContext } from "react";

import OtpInput from "react-otp-input";
import classNames from "./settingsModal.module.scss";
import arrowGo from "../../static/images/clipIcons/arrowGo.svg";
import "./adminModal.scss";
import { toast } from "react-toastify";
// import { useAllBankers } from "../../queryHooks";
import axios from "axios";

// import { useContext } from "react";
import { GlobalContex } from "../../globalContex";

function AdminModal({ onClose = () => {}, onSuccess = () => {}, logoParam }) {
  const {
    bankerEmail,
    loginData,
    setBankerEmail,
    selectedApp,
    shareToken,
    setShareToken,
    banker,
    setBanker,
    allShareToken,
    setAllShareToken,
    allBankers,
    setAllBankers,
    setModalOpen,
  } = useContext(GlobalContex);

  const [step, setStep] = useState("");
  const [pin, setPin] = useState("");
  // const { data: bankerList } = useAllBankers();
  const [query, setQuery] = useState("");
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    setLoading(true);
    axios
      .get(
        `https://comms.globalxchange.com/coin/investment/path/get?investmentType=EQT&banker_email=${localStorage.getItem(
          "bankerEmailNew"
        )}`
      )
      .then((res) => {
        if (res.data.status) {
          setLoading(false);
          setAllShareToken(res.data.paths);
          console.log(res.data.paths, "jhvdjhwvedhjwed");
        } else {
          setAllShareToken([]);
          setLoading(false);
        }
      });
  }, [localStorage.getItem("bankerEmailNew")]);

  useEffect(() => {
    // setAllShareToken([]);
    axios.get(`https://teller2.apimachine.com/admin/allBankers`).then((res) => {
      setAllBankers(res.data.data);
    });
  }, []);

  const filteredSharedToken = allShareToken
    ? allShareToken.filter((item) => {
        const lowquery = query.toLowerCase();
        return (
          (item.token_profile_data.coinSymbol + item.apps_metadata[0].app_name)
            .toLowerCase()
            .indexOf(lowquery) >= 0
        );
      })
    : "";

  const filteredBankers = allBankers
    ? allBankers.filter((item) => {
        const lowquery = query.toLowerCase();
        return (
          (item.displayName + item.email).toLowerCase().indexOf(lowquery) >= 0
        );
      })
    : "";

  useEffect(() => {
    if (pin === "9605") {
      setStep("changeAdmin");
    } else if (pin.length === 4) {
      toast.error("Invalid Pin");
      setPin("");
    }
  }, [pin]);

  const conditionalMenu = () => {
    if (selectedApp.appName === "Banker") {
      return (
        <>
          <div className={classNames.inCard}>
            <div
              className={classNames.title}
              style={{
                fontSize: "30px",
                fontWeight: 800,
                color: "#18191D",
                textAlign: "center",
                padding: "20px 0px",
              }}
            >
              <img
                src={logoParam || selectedApp.appFullLogo}
                alt=""
                className={classNames.logo}
                style={{ border: "none" }}
              />
            </div>

            <div
              style={{
                padding: "0px 40px",
                overflowY: "scroll",
                // height: "400px",
              }}
            >
              <div
                className="coin-card"
                style={{ height: "90px" }}
                onClick={(e) => {}}
              >
                <div>Banker Profile</div>
                <img src={arrowGo} alt="" width="20px" />
              </div>

              <div
                className="coin-card"
                style={{ height: "90px" }}
                onClick={(e) => setStep("Admin")}
              >
                <div>
                  <div>Admin</div>
                  <div
                    style={{
                      fontWeight: 400,
                      fontSize: "14px",
                      paddingTop: "3px",
                    }}
                  >
                    Currently Selected:{" "}
                    {banker
                      ? banker.bankerTag
                      : localStorage.getItem("bankerTag")}
                  </div>
                </div>
                <img src={arrowGo} alt="" width="20px" />
              </div>
            </div>
          </div>
          <div className={classNames.footerBtns}>
            <div
              className={classNames.btnSettings}
              style={{ background: selectedApp.appColor }}
            >
              <span>Refer A Banker</span>
            </div>
          </div>
        </>
      );
    } else if (selectedApp.appName === "Capitalized") {
      return (
        <>
          <div className={classNames.inCard}>
            <div
              className={classNames.title}
              style={{
                fontSize: "30px",
                fontWeight: 800,
                color: "#18191D",
                textAlign: "center",
                padding: "20px 0px",
              }}
            >
              <img
                src={logoParam || selectedApp.appFullLogo}
                alt=""
                className={classNames.logo}
                style={{ border: "none" }}
              />
            </div>

            <div
              style={{
                padding: "0px 40px",
                overflowY: "scroll",
                height: "300px",
              }}
            >
              <div
                className="coin-card"
                style={{ height: "90px" }}
                onClick={(e) => {}}
              >
                <div>Banker Profile</div>
                <img src={arrowGo} alt="" width="20px" />
              </div>
              <div
                className="coin-card"
                style={{ height: "90px" }}
                onClick={(e) => {}}
              >
                <div>Company Profile</div>
                <img src={arrowGo} alt="" width="20px" />
              </div>
              <div
                className="coin-card"
                style={{ height: "90px" }}
                onClick={(e) => setStep("changeCompany")}
              >
                <div>
                  <div>Change Company</div>

                  <div
                    style={{
                      fontWeight: 400,
                      fontSize: "14px",
                      paddingTop: "3px",
                    }}
                  >
                    Currently Selected:{" "}
                    {shareToken
                      ? shareToken.coinSymbol
                      : localStorage.getItem("shareToken")
                      ? localStorage.getItem("shareToken")
                      : ""}
                  </div>
                </div>
                <img src={arrowGo} alt="" width="20px" />
              </div>
              <div
                className="coin-card"
                style={{ height: "90px" }}
                onClick={(e) => setStep("Admin")}
              >
                <div>
                  <div>Admin</div>
                  <div
                    style={{
                      fontWeight: 400,
                      fontSize: "14px",
                      paddingTop: "3px",
                    }}
                  >
                    Currently Selected:{" "}
                    {banker
                      ? banker.bankerTag
                      : localStorage.getItem("bankerTag")}
                  </div>
                </div>
                <img src={arrowGo} alt="" width="20px" />
              </div>
            </div>
          </div>
          <div className={classNames.footerBtns}>
            <div
              className={classNames.btnSettings}
              style={{ background: selectedApp.appColor }}
            >
              <span>Refer A Banker</span>
            </div>
          </div>
        </>
      );
    }
  };

  const stepRender = useMemo(() => {
    switch (step) {
      case "Admin":
        return (
          <>
            <div className={classNames.inCard}>
              <img
                src={logoParam || selectedApp.appFullLogo}
                alt=""
                className={classNames.logo}
              />
              <div className={classNames.otpView}>
                <div className={classNames.label}>Enter Admin PIN</div>
                <OtpInput
                  value={pin}
                  onChange={setPin}
                  numInputs={4}
                  separator={<span> </span>}
                  shouldAutoFocus
                  containerStyle={classNames.otpInputWrapper}
                  inputStyle={classNames.otpInput}
                />
              </div>
            </div>
            <div className={classNames.footerBtns}>
              <div
                style={{ background: selectedApp.appColor }}
                className={classNames.btnSettings}
                onClick={() => {
                  setStep("");
                }}
              >
                <span>Back</span>
              </div>
            </div>
          </>
        );
      case "AdminBankers":
        return (
          <>
            <div className={classNames.inCard}>
              <img
                src={logoParam || selectedApp.appFullLogo}
                alt=""
                className={classNames.logo}
              />
              <div
                className={`${classNames.settingCards} ${classNames.scroll}`}
              >
                {allBankers.map((banker) => (
                  <div
                    className={classNames.settingCard}
                    key={banker?._id}
                    onClick={() => {
                      setBankerEmail(banker?.email);

                      onClose();
                    }}
                  >
                    <img
                      src={banker?.profilePicURL}
                      alt=""
                      className={classNames.icon}
                    />
                    <span>{banker?.displayName}</span>
                  </div>
                ))}
                <div
                  className={classNames.settingCard}
                  style={{ visibility: "hidden" }}
                />
                <div
                  className={classNames.settingCard}
                  style={{ visibility: "hidden" }}
                />
              </div>
            </div>
            <div className={classNames.footerBtns}>
              <div
                style={{ background: selectedApp.appColor }}
                className={classNames.btnSettings}
                onClick={() => {
                  try {
                    if (loginData.user.email) {
                      localStorage.clear();
                      onClose();
                    } else onSuccess();
                  } catch (error) {}
                }}
              >
                <span>{loginData.user.email ? "Logout" : "Login"}</span>
              </div>
            </div>
          </>
        );
      case "changeCompany":
        return (
          <>
            <div className={classNames.inCard}>
              <div
                className={classNames.title}
                style={{
                  fontSize: "30px",
                  fontWeight: 800,
                  color: "#18191D",
                  textAlign: "center",
                  padding: "20px 0px",
                }}
              >
                <img
                  src={logoParam || selectedApp.appFullLogo}
                  alt=""
                  className={classNames.logo}
                  style={{ border: "none" }}
                />
              </div>
              <div style={{ padding: "0px 40px", paddingBottom: "40px" }}>
                <input
                  value={query}
                  onChange={(e) => setQuery(e.target.value)}
                  type="text"
                  placeholder="Search For A ShareToken ..."
                  style={{
                    border: "0.5px solid #E5E5E5",
                    borderRadius: "15px",
                    height: "86px",
                    width: "100%",
                    padding: "0px 30px",
                    fontSize: "18px",
                    fontWeight: "700",
                  }}
                />
              </div>

              <div
                style={{
                  padding: "0px 40px",
                  overflowY: "scroll",
                  height: "400px",
                  paddingBottom: "100px",
                }}
              >
                {!loading ? (
                  filteredSharedToken.length > 0 ? (
                    filteredSharedToken.map((item) => {
                      return (
                        <div
                          className="coin-card"
                          style={{ height: "90px" }}
                          onClick={(e) => {
                            setShareToken(item.token_profile_data);
                            localStorage.setItem(
                              "shareToken",
                              item.token_profile_data.coinSymbol
                            );
                            localStorage.setItem(
                              "shareTokenData",
                              JSON.stringify(item.token_profile_data)
                            );
                            setQuery("");
                            onClose();
                          }}
                        >
                          <div
                            style={{ display: "flex", alignItems: "center" }}
                          >
                            <div style={{ paddingRight: "20px" }}>
                              <img
                                src={item.token_profile_data.coinImage}
                                alt=""
                                width="30px"
                                style={{ borderRadius: "50%" }}
                              />
                            </div>
                            <div>
                              <div>{item.apps_metadata[0].app_name}</div>
                              <div
                                style={{ fontWeight: 400, fontSize: "12px" }}
                              >
                                {item.token_profile_data.coinSymbol}
                              </div>
                            </div>
                          </div>
                        </div>
                      );
                    })
                  ) : (
                    <div
                      style={{
                        width: "100%",
                        height: "20vh",
                        display: "flex",
                        justifyContent: "center",
                        alignItems: "center",
                      }}
                    >
                      No ShareToken Found...
                    </div>
                  )
                ) : (
                  <div
                    style={{
                      width: "100%",
                      height: "20vh",
                      display: "flex",
                      justifyContent: "center",
                      alignItems: "center",
                    }}
                  >
                    Loading...
                  </div>
                )}
              </div>
            </div>
            <div
              className={classNames.footerBtns}
              style={{ position: "absolute", bottom: 0 }}
            >
              {step === "changeCompany" ? (
                <div
                  className={classNames.btnSettings}
                  onClick={(e) => setStep(null)}
                  style={{ background: selectedApp.appColor }}
                >
                  <span>Go Back</span>
                </div>
              ) : (
                <div
                  className={classNames.btnSettings}
                  style={{ background: selectedApp.appColor }}
                >
                  <span>Refer A Banker</span>
                </div>
              )}
            </div>
          </>
        );
      case "changeAdmin":
        return (
          <>
            <div className={classNames.inCard}>
              <div
                className={classNames.title}
                style={{
                  fontSize: "30px",
                  fontWeight: 800,
                  color: "#18191D",
                  textAlign: "center",
                  padding: "0px 0px",
                }}
              >
                <img
                  src={logoParam || selectedApp.appFullLogo}
                  alt=""
                  className={classNames.logo}
                  style={{ border: "none" }}
                />
              </div>
              <div style={{ padding: "0px 40px", paddingBottom: "40px" }}>
                <input
                  value={query}
                  onChange={(e) => setQuery(e.target.value)}
                  type="text"
                  placeholder="Search For A Banker ..."
                  style={{
                    border: "0.5px solid #E5E5E5",
                    borderRadius: "15px",
                    height: "86px",
                    width: "100%",
                    padding: "0px 30px",
                    fontSize: "18px",
                    fontWeight: "700",
                  }}
                />
              </div>

              <div
                style={{
                  padding: "0px 40px",
                  overflowY: "scroll",
                  height: "280px",
                  paddingBottom: "50px",
                }}
              >
                {filteredBankers.map((item) => {
                  return (
                    <div
                      // onClick={(e) => {
                      //   // e.stopPropagation();
                      //   console.log("inside onclick", item);
                      // }}
                      onClick={() => {
                        setBankerEmail(item.email);
                        // setBankerEmail(banker?.email);
                        if (selectedApp.appName !== "Banker") {
                          setStep("changeCompany");
                          localStorage.setItem("bankerEmailNew", item.email);
                          localStorage.setItem("bankerTag", item.bankerTag);
                          localStorage.setItem("banker", JSON.stringify(item));
                          setBanker(item);
                        } else {
                          setModalOpen(false);
                          localStorage.setItem("bankerEmailNew", item.email);
                          localStorage.setItem("bankerTag", item.bankerTag);
                          localStorage.setItem("banker", JSON.stringify(item));
                          setBanker(item);
                        }
                      }}
                      className="coin-card"
                      style={{ height: "90px" }}
                    >
                      <div style={{ display: "flex", alignItems: "center" }}>
                        <div style={{ paddingRight: "20px" }}>
                          <img
                            src={item.profilePicURL}
                            alt=""
                            width="30px"
                            style={{ borderRadius: "50%" }}
                          />
                        </div>
                        <div>
                          <div>{item.displayName}</div>
                          <div style={{ fontWeight: 400, fontSize: "12px" }}>
                            {item.email}
                          </div>
                        </div>
                      </div>
                    </div>
                  );
                })}
              </div>
            </div>
            <div
              className={classNames.footerBtns}
              // style={{ position: "absolute", bottom: 0 }}
            >
              <div
                className={classNames.btnSettings}
                style={{ background: selectedApp.appColor }}
              >
                <span>Refer A Banker</span>
              </div>
            </div>
          </>
        );
      default:
        return conditionalMenu();
    }
  }, [
    loginData.user.email,
    logoParam,
    onClose,
    onSuccess,
    pin,
    step,
    selectedApp,
  ]);

  return (
    <>
      <div className={classNames.settingsModal}>
        <div
          className={classNames.overlayClose}
          onClick={() => {
            try {
              onClose();
            } catch (error) {}
          }}
        />
        <div className={classNames.settingsCard}>{stepRender}</div>
      </div>
    </>
  );
}

export default AdminModal;
