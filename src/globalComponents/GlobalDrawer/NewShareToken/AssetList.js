import Axios from "axios";
import React, { Fragment, useEffect, useState } from "react";
import Scrollbars from "react-custom-scrollbars";
import Skeleton from "react-loading-skeleton";

function AssetList({ setIssuanceAsset, onClose }) {
  const [assets, setAssets] = useState([]);
  const [assetLoading, setAssetLoading] = useState(true);
  useEffect(() => {
    Axios.get("https://comms.globalxchange.com/coin/vault/get/all/coins")
      .then((res) => {
        if (res.data.status) setAssets(res.data.coins);
      })
      .finally(() => setAssetLoading(false));
  }, []);
  const [search, setSearch] = useState("");
  return (
    <Fragment>
      <div className="titleOp">Select Issuance Asset</div>
      <div className="searchWrap">
        <input
          value={search}
          onChange={(e) => setSearch(e.target.value)}
          type="text"
          placeholder="Search Assets....|"
        />
      </div>
      <Scrollbars className="searchList">
        {assetLoading
          ? Array(6)
              .fill("")
              .map((_, i) => (
                <div className="user" key={i}>
                  <Skeleton className="dp" circle />
                  <div className="userDetail">
                    <Skeleton className="name" width={200} />
                    <Skeleton className="email" width={200} />
                  </div>
                </div>
              ))
          : assets
              .filter(
                (asset) =>
                  asset.coinName
                    ?.toLowerCase()
                    .includes(search.toLowerCase()) ||
                  asset.coinSymbol?.toLowerCase().includes(search.toLowerCase())
              )
              .map((asset) => (
                <div
                  className="user"
                  key={asset._id}
                  onClick={() => {
                    setIssuanceAsset(asset);
                    onClose();
                  }}
                >
                  <img className="dp" src={asset.coinImage} alt="" />
                  <div className="userDetail">
                    <div className="name">{asset.coinName}</div>
                    {/* <div className="email">{banker.email}</div> */}
                  </div>
                </div>
              ))}
        <div className="space"></div>
      </Scrollbars>
    </Fragment>
  );
}

export default AssetList;
