import Axios from "axios";
import React, { Fragment, useContext, useEffect, useState } from "react";
import Scrollbars from "react-custom-scrollbars";
import Skeleton from "react-loading-skeleton";
import { GlobalContex } from "../../../globalContex";

function CompanyList({ setCompany, onClose }) {
  const { bankerEmail } = useContext(GlobalContex);

  const [allCompany, setAllCompany] = useState([]);
  const [assetLoading, setAssetLoading] = useState(true);

  useEffect(() => {
    Axios.get(
      `https://comms.globalxchange.com/gxb/apps/get?created_by=${bankerEmail}`
    )
      .then((res) => {
        if (res.data.status) setAllCompany(res.data.apps);
      })
      .finally(() => setAssetLoading(false));
  }, []);
  const [search, setSearch] = useState("");
  return (
    <Fragment>
      <div className="titleOp">Select One Of Your Companies</div>
      <div className="searchWrap">
        <input
          value={search}
          onChange={(e) => setSearch(e.target.value)}
          type="text"
          placeholder="Search Assets....|"
        />
      </div>
      <Scrollbars className="searchList">
        {assetLoading
          ? Array(6)
              .fill("")
              .map((_, i) => (
                <div className="user" key={i}>
                  <Skeleton className="dp" circle />
                  <div className="userDetail">
                    <Skeleton className="name" width={200} />
                    <Skeleton className="email" width={200} />
                  </div>
                </div>
              ))
          : allCompany
              .filter(
                (item) =>
                  item.app_name?.toLowerCase().includes(search.toLowerCase()) ||
                  item.app_code?.toLowerCase().includes(search.toLowerCase())
              )
              .map((item) => (
                <div
                  className="user"
                  key={item._id}
                  onClick={() => {
                    setCompany(item);
                    onClose();
                  }}
                >
                  <img className="dp" src={item.app_icon} alt="" />
                  <div className="userDetail">
                    <div className="name">{item.app_name}</div>
                    {/* <div className="email">{banker.email}</div> */}
                  </div>
                </div>
              ))}
        <div className="space"></div>
      </Scrollbars>
    </Fragment>
  );
}

export default CompanyList;
