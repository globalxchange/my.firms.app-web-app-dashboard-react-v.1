import { memo, useEffect, useMemo, useState, useContext } from "react";
import { motion } from "framer-motion";
import classNames from "./globalNewSidebar.module.scss";
import backWhite from "../../static/images/icons/backWhite.svg";
import closeWhite from "../../static/images/icons/closeWhite.svg";
import bondTier from "../../static/images/icons/bondTier.svg";
import offerings from "../../static/images/sidebarIcons/offerings.svg";
import shares from "../../static/images/sidebarIcons/shares.svg";
import bonds from "../../static/images/sidebarIcons/bonds.svg";

import NewShareToken from "./NewShareToken";
import { GlobalContex } from "../../globalContex";

function GlobalDrawer({ onClose, pathname }) {
  const { globalMenuAdd, setGlobalMenuAdd } = useContext(GlobalContex);

  const [search, setSearch] = useState("");
  const [mainMenu, setMainMenu] = useState("");
  const [subMenu, setSubMenu] = useState("");
  const [step, setStep] = useState("");

  const [menuList, setMenuList] = useState([]);

  useEffect(() => {
    console.log(pathname, "jbdkjwqed");
    if (pathname === "/Banker") {
      setMenuList([
        {
          name: "Bond",
          icon: bonds,
        },
        {
          name: "Bond Tier",
          icon: bondTier,
        },
        {},
      ]);
    } else if (pathname === "/Capitalized") {
      setMenuList([
        {
          name: "ShareToken",
          icon: offerings,
        },
        {
          name: "Shares",
          icon: shares,
        },
        {},
      ]);
    }
  }, []);

  const content = useMemo(() => {
    switch (mainMenu) {
      case "ShareToken":
        return <NewShareToken step={step} setStep={setStep} />;

      default:
        return (
          <>
            <label className={classNames.searchBox}>
              <input
                type="text"
                placeholder="Search For New Item To Add.."
                value={search}
                onChange={(e) => setSearch(e.target.value)}
              />
            </label>
            <div className={classNames.verticalScrollBox}>
              {menuList.map(({ name, icon }) => (
                <ItemBox
                  name={name}
                  icon={icon}
                  onClick={() => setMainMenu(name)}
                />
              ))}
            </div>
          </>
        );
    }
  }, [mainMenu, step, setStep, menuList]);

  return (
    <div className={classNames.GlobalDrawer}>
      <div
        className={classNames.overlay}
        onClick={() => {
          try {
            onClose();
          } catch (error) {}
        }}
      />
      <div
        // initial={{ x: "32%", opacity: 0 }}
        // animate={{ x: 0, opacity: 1 }}
        // exit={{ x: "32%", opacity: 0 }}
        className={classNames.sidebar}
        // style={{ width: "300px", position: "fixed", right: 0 }}
      >
        <div className={classNames.header}>
          <div className={classNames.content}>
            <div className={classNames.title}>New Item</div>
            <div className={classNames.breadCrumbs}>
              <span
                onClick={() => {
                  setMainMenu("");
                }}
              >
                Add New
              </span>
              {mainMenu && (
                <>
                  -&gt;&nbsp;
                  <span
                    onClick={() => {
                      setStep("");
                    }}
                  >
                    {mainMenu}
                  </span>
                </>
              )}
              {step && (
                <>
                  -&gt;&nbsp;
                  <span>{step}</span>
                </>
              )}
            </div>
          </div>
          <div
            className={classNames.backBtn}
            onClick={(e) => {
              if (step !== "") {
                setStep("");
              } else {
                setMainMenu("");
              }
            }}
          >
            <img src={backWhite} alt="" />
          </div>
          <div
            className={classNames.closeBtn}
            onClick={() => {
              try {
                setGlobalMenuAdd(false);
              } catch (error) {}
            }}
          >
            <img src={closeWhite} alt="" />
          </div>
        </div>

        {content}
      </div>
    </div>
  );
}

function ItemBox({ name, icon, onClick }) {
  return (
    <div
      className={classNames.itemBox}
      style={{
        visibility: name ? "unset" : "hidden",
      }}
      onClick={onClick}
    >
      <motion.svg
        viewBox="0 0 123 123"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
        className={classNames.svgFrame}
      >
        <rect
          x="0.75"
          y="0.75"
          width="121.5"
          height="121.5"
          rx="15.25"
          fill="white"
          stroke="#E7E7E7"
          stroke-width="0.5"
        />
        <foreignObject
          x="0.75"
          y="0.75"
          width="121.5"
          height="121.5"
          rx="15.25"
        >
          <div className={classNames.iconWrap}>
            <img src={icon} alt="" />
          </div>
        </foreignObject>
      </motion.svg>
      <div className={classNames.label} style={{ paddingTop: "10px" }}>
        {name}
      </div>
    </div>
  );
}

export default memo(GlobalDrawer);
