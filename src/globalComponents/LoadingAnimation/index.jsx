import { memo, useContext } from "react";
import { motion } from "framer-motion";
import { useLottie } from "lottie-react";
import eventMapLottie from "../../static/animations/eventMapLottie.json";
import classNames from "./loadingAnimation.module.scss";
import { GlobalContex } from "../../globalContex";

function LoadingAnimation({ className, classNameSvg, logoAnim = false }) {
  const { selectedApp } = useContext(GlobalContex);

  const icon = {
    hidden: {
      opacity: 0,
      pathLength: 0,
      fill: "#18191D00",
    },
    visible: {
      opacity: 1,
      pathLength: 1,
      fill: selectedApp.appColor,
    },
  };

  const options = {
    animationData: eventMapLottie,
    loop: true,
    autoplay: true,
    className: classNames.loadingAnimation,
    // className: `${classNames.logo} ${classNameSvg}`,
  };
  const { View } = useLottie(options);

  const conditionalPaths = () => {
    if (selectedApp?.appName === "Banker") {
      return (
        <motion.svg
          className={classNames.animation}
          // className={`${classNames.logo} ${classNameSvg}`}
          viewBox="0 0 80 80"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <motion.path
            stroke={selectedApp.appColor}
            strokeWidth={0.5}
            variants={icon}
            initial="hidden"
            animate="visible"
            transition={{
              default: { duration: 2, ease: "easeInOut", repeat: Infinity },
              fill: { duration: 2, ease: [1, 0, 0.8, 1], repeat: Infinity },
            }}
            d="M44.0252 44.7317C46.6088 38.6842 45.5735 31.7916 41.203 26.9424C40.4984 26.1614 40.3678 25.6327 40.9108 24.6361C47.0597 13.5151 39.5748 0.0600252 27.2533 0.0200216C25.2776 0.0200216 23.302 0.0513287 21.3264 0.000889521C20.5147 -0.0182426 20.2592 0.268739 20.2592 1.10533C20.296 4.27604 20.301 7.44851 20.2592 10.6209C20.2459 11.5236 20.5181 11.8019 21.3831 11.788C24.4276 11.7393 27.4737 11.7567 30.5198 11.788C31.021 11.788 31.8307 11.5201 31.5687 12.6246L31.5551 20.2774C31.6587 21.2549 30.944 20.9923 30.4847 20.9958C27.5205 21.0166 24.5562 21.0427 21.5936 20.9836C20.5799 20.9627 20.2292 21.2706 20.2576 22.3576C20.3244 25.0987 20.2726 27.8451 20.2843 30.5879C20.2843 32.6404 21.0675 33.4316 23.0699 33.4368C26.3214 33.4368 29.5746 33.4698 32.8245 33.4213C33.8014 33.4074 34.1305 33.769 34.1069 34.7622C34.0569 36.9903 34.0603 39.2199 34.1069 41.4498C34.1254 42.4027 33.7847 42.7246 32.8778 42.714C29.4627 42.6759 26.0442 42.7349 22.629 42.6916C16.2996 42.6117 11.4549 37.5989 11.4232 31.007C11.3742 21.1906 11.3781 11.3735 11.4348 1.55581C11.4348 0.305265 11.1008 -0.0234605 9.92178 0.004368C6.63185 0.0826358 3.33856 0.0235003 0.0469499 0.0182824C0.0469499 11.628 -0.0783019 23.2412 0.0803503 34.8491C0.212282 44.589 7.77415 53.016 17.0829 54.1552C21.6303 54.7117 26.2062 54.4804 30.762 54.3117C34.0518 54.19 36.9661 52.7898 39.563 50.6853C39.563 50.668 43.0535 47.2293 44.0252 44.7317Z"
            fill={selectedApp.appColor}
          />
        </motion.svg>
      );
    } else if (selectedApp?.appName === "Capitalized") {
      return (
        <motion.svg
          // className={`${classNames.logo} ${classNameSvg}`}
          viewBox="0 0 80 80"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <motion.path
            stroke={selectedApp.appColor}
            strokeWidth={0.5}
            variants={icon}
            initial="hidden"
            animate="visible"
            transition={{
              default: { duration: 2, ease: "easeInOut", repeat: Infinity },
              fill: { duration: 2, ease: [1, 0, 0.8, 1], repeat: Infinity },
            }}
            fill-rule="evenodd"
            clip-rule="evenodd"
            d="M28.4249 0C32.0239 0 35.4387 0.677207 38.6072 1.87792C36.7002 1.41691 34.7003 1.20073 32.6394 1.20073C18.304 1.20073 6.7063 12.7989 6.7063 27.1346C6.7063 41.4701 18.304 53.0685 32.6394 53.0685C40.1762 53.0685 46.9441 49.8674 51.6816 44.7625C46.544 52.0527 38.0227 56.8191 28.4249 56.8191C12.7359 56.8191 0 44.1165 0 28.4264C0 12.7052 12.7359 0 28.4249 0Z"
            fill={selectedApp.appColor}
          />
          <motion.path
            stroke={selectedApp.appColor}
            strokeWidth={0.5}
            variants={icon}
            initial="hidden"
            animate="visible"
            transition={{
              default: { duration: 2, ease: "easeInOut", repeat: Infinity },
              fill: { duration: 2, ease: [1, 0, 0.8, 1], repeat: Infinity },
            }}
            fill-rule="evenodd"
            clip-rule="evenodd"
            d="M44.1141 11.5383L55.6192 4.40177L41.6836 52.7585L31.4702 55.5271L44.1141 11.5383Z"
            fill={selectedApp.appColor}
          />
          <motion.path
            stroke={selectedApp.appColor}
            strokeWidth={0.5}
            variants={icon}
            initial="hidden"
            animate="visible"
            transition={{
              default: { duration: 2, ease: "easeInOut", repeat: Infinity },
              fill: { duration: 2, ease: [1, 0, 0.8, 1], repeat: Infinity },
            }}
            fill-rule="evenodd"
            clip-rule="evenodd"
            d="M9.78263 32.5781L21.2879 25.4416L15.0121 47.1925L6.27582 44.8535L9.78263 32.5781Z"
            fill={selectedApp.appColor}
          />
          <motion.path
            stroke={selectedApp.appColor}
            strokeWidth={0.5}
            variants={icon}
            initial="hidden"
            animate="visible"
            transition={{
              default: { duration: 2, ease: "easeInOut", repeat: Infinity },
              fill: { duration: 2, ease: [1, 0, 0.8, 1], repeat: Infinity },
            }}
            fill-rule="evenodd"
            clip-rule="evenodd"
            d="M26.9482 22.0583L38.392 15.1353L26.7636 55.5271L18.027 53.097L26.9482 22.0583Z"
            fill={selectedApp.appColor}
          />
        </motion.svg>
      );
    }
  };

  return (
    <div
      style={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        height: "100vh",
        paddingLeft: "10%",
        paddingTop: "25%",
      }}
    >
      {logoAnim ? conditionalPaths() : View}
    </div>
  );
}

export default memo(LoadingAnimation);
