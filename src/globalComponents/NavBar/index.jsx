import React from "react";
import { useState, useContext } from "react";
import FilterType from "../Filters/FilterType";
import UserFilter from "../Filters/UserFilter";
import CoinFilter from "../Filters/CoinFilter";
// import RightButton from "../RightButton";
import "./nav.scss";
import { GlobalContex } from "../../globalContex";
import { useEffect } from "react";

const NavBar = ({
  logo,
  name,
  tabs,
  tabSelected,
  setTabSelected,
  enabledFilters,
  customFilters,
}) => {
  //   const [tabSelected, setTabSelected] = useState(null);
  
  const {
    selectedFilter1,
    setSelectedFilter1,
    selectedFilter2,
    setSelectedFilter2,
    selectedFilter21,
    setSelectedFilter21,
    filter1,
    setFilter1,
    filter2,
    setFilter2,
    customerEmailFilter,
    setCustomerEmailFilter,
    allCoins,
    setAllCoins,
    selectedCoin,
    setSelectedCoin,
    openCoinFilter,
    setOpenCoinFilter,
    refetchApi,
    setRefetchApi,
    setShowSubDraw,
  } = useContext(GlobalContex);
  const [query, setQuery] = useState("");

  // useEffect(() => {
  //   setShowSubDraw(false);
  // }, [tabSelected]);

  return (
    <>
      <div style={{ borderBottom: "#e7e7e780 solid 1.5px" }}>
        <div
          style={{
            display: "flex",
            justifyContent: "space-between",
            // alignItems: "center",
          }}
        >
          <div className="tab">
            {tabs.map((tabItm) => (
              <div
                key={tabItm}
                className={
                  "tabitm" + " " + (tabSelected === tabItm ? "true" : "")
                }
                onClick={() => {
                  setTabSelected(tabItm);
                  console.log(tabItm, "jhwdjwed");
                  //   tabClick();
                }}
              >
                <h6 style={{ flexWrap: "nowrap" }}>{tabItm}</h6>
              </div>
            ))}
          </div>

          <div style={{ display: "flex" }}>
            {customFilters ? customFilters : ""}
            <div
              className={
                enabledFilters[0] ? "coin-button nav-user" : "nav-user"
              }
              onClick={(e) => (enabledFilters[0] ? setFilter1(true) : "")}
            >
              {selectedFilter1 ? (
                <img
                  style={{ opacity: enabledFilters[0] ? 1 : 0.3 }}
                  src={selectedFilter1.icon}
                  alt=""
                  width={20}
                />
              ) : (
                <img
                  style={{ opacity: enabledFilters[0] ? 1 : 0.3 }}
                  src={require("../../static/images/icons/user.svg").default}
                  alt=""
                  width="20px"
                />
              )}
            </div>
            <div
              className={
                enabledFilters[1] ? "coin-button nav-user" : "nav-user"
              }
              onClick={(e) => (enabledFilters[1] ? setFilter2(true) : "")}
            >
              {selectedFilter21 ? (
                <img
                  style={{
                    opacity: enabledFilters[1] ? 1 : 0.3,
                    borderRadius: "50%",
                  }}
                  src={selectedFilter21.profile_img}
                  alt=""
                  width="22px"
                />
              ) : (
                <img
                  style={{ opacity: enabledFilters[1] ? 1 : 0.3 }}
                  src={require("../../static/images/icons/all.svg").default}
                  alt=""
                />
                // <span>
                //   All {selectedFilter1 ? selectedFilter1.name : "User"}
                // </span>
              )}
            </div>
            <div
              className={
                enabledFilters[2] ? "coin-button nav-user" : "nav-user"
              }
              onClick={(e) =>
                enabledFilters[2] ? setOpenCoinFilter(true) : ""
              }
            >
              <img
                style={{ opacity: enabledFilters[2] ? 1 : 0.3 }}
                src={
                  selectedCoin ? selectedCoin.coinImage : allCoins[0]?.coinImage
                }
                alt=""
                width="21px"
              />
            </div>
            <div
              className={
                enabledFilters[3] ? "coin-button nav-user" : "nav-user"
              }
            >
              <img
                style={{ opacity: enabledFilters[3] ? 1 : 0.3 }}
                src={require("../../static/images/icons/refresh.svg").default}
                alt=""
              />
            </div>
            <div
              className={
                enabledFilters[3] ? "coin-button nav-user" : "nav-user"
              }
            >
              <img
                style={{ opacity: enabledFilters[4] ? 1 : 0.3 }}
                src={require("../../static/images/icons/search.svg").default}
                alt=""
              />
            </div>
          </div>
        </div>
      </div>

      {filter1 ? (
        <FilterType
          onClose={() => setFilter1(false)}
          onSuccess={() => setFilter1(false)}
          allCoins={allCoins}
          filter1={filter1}
          setFilter1={setFilter1}
          selectedCoin={selectedCoin}
          setSelectedCoin={setSelectedCoin}
          selectedFilter1={selectedFilter1}
          setSelectedFilter1={setSelectedFilter1}
          tabSelected={tabSelected}
          setSelectedFilter21={setSelectedFilter21}
          selectedFilter21={selectedFilter21}
        />
      ) : (
        ""
      )}
      {filter2 ? (
        <UserFilter
          onClose={() => setFilter2(false)}
          onSuccess={() => setFilter2(false)}
          allCoins={allCoins}
          filter2={filter2}
          setFilter2={setFilter2}
          selectedCoin={selectedCoin}
          setSelectedCoin={setSelectedCoin}
          selectedFilter2={selectedFilter2}
          setSelectedFilter2={setSelectedFilter2}
          selectedFilter21={selectedFilter21}
          setSelectedFilter21={setSelectedFilter21}
          customerEmailFilter={customerEmailFilter}
          setCustomerEmailFilter={setCustomerEmailFilter}
          refetchApi={refetchApi}
          setRefetchApi={setRefetchApi}
          query={query}
          setQuery={setQuery}
          tabSelected={tabSelected}
        />
      ) : (
        ""
      )}

      {openCoinFilter ? (
        <CoinFilter
          onClose={() => setOpenCoinFilter(false)}
          onSuccess={() => setOpenCoinFilter(false)}
          allCoins={allCoins}
          openCoinFilter={openCoinFilter}
          setOpenCoinFilter={setOpenCoinFilter}
          selectedCoin={selectedCoin}
          setSelectedCoin={setSelectedCoin}
        />
      ) : (
        ""
      )}
    </>
  );
};

export default NavBar;
