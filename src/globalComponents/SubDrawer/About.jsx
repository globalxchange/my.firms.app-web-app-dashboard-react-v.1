import React, { useContext, useEffect, useState } from "react";

import back from "../../static/images/icons/back.svg";
import close from "../../static/images/icons/close1.svg";
import editApp from "../../static/images/clipIcons/appData.svg";
import userIcon from "../../static/images/clipIcons/userIcon.svg";
import deleteApp from "../../static/images/clipIcons/delete.svg";
import bondIcon from "../../static/images/clipIcons/bondIcon.svg";
import plusIcon from "../../static/images/clipIcons/plus.svg";
import closeIcon from "../../static/images/clipIcons/no.svg";
import defaultImg from "../../static/images/icons/app_placeholder.png";
import { GlobalContex } from "../../globalContex";

const About = () => {
  const { showSubDraw, setShowSubDraw } = useContext(GlobalContex);
  const [step, setStep] = useState(null);
  const [path, setPath] = useState([]);

  const defaultCase = [
    {
      name: "name 1",
      icon: editApp,
    },
    {
      name: "name 2",
      icon: userIcon,
    },
    {
      name: "name 3",
      icon: deleteApp,
    },
    {
      name: "name 4",
      icon: bondIcon,
    },
    {
      name: "name 4",
      icon: bondIcon,
    },
    {
      name: "name 4",
      icon: bondIcon,
    },
    {
      name: "name 4",
      icon: bondIcon,
    },
    {
      name: "name 4",
      icon: bondIcon,
    },
  ];

  useEffect(() => {
    setStep("About");
    setPath(["About"]);
  }, []);

  const handleBackStep = () => {
    if (path.length > 1) {
      path.splice(-1);
      // console.log(tempPath, "kqjdkjwed");
      setPath([...path]);
      if (path.length > 0) {
        setStep(path[path.length - 1]);
      }
    }
  };

  const handleBreadcrumbClick = (clickedStep) => {
    const foundStepIndex = path.findIndex((o) => o === clickedStep);
    const tempPath = path;
    tempPath.splice(foundStepIndex + 1, tempPath.length);
    setPath([...tempPath]);
    console.log(path, "lwndwed");
  };

  useEffect(() => {
    setStep(path[path.length - 1]);
  }, [path]);

  const conditionalSteps = () => {
    switch (step) {
      case "step1":
        return (
          <>
            <div className="sidebarTitle">This is the 1st step </div>
            <div
              className="sidebarCard"
              onClick={(e) => {
                setStep("step2");
                setPath([...path, "step2"]);
              }}
            >
              <img src={editApp} alt="" style={{ width: "20px" }} />
              <div style={{ paddingLeft: "10px" }}>Item 1 of Step 1</div>
            </div>
          </>
        );

      case "step2":
        return (
          <>
            <div className="sidebarTitle">This is the 2nd step </div>
            <div
              className="sidebarCard"
              onClick={(e) => {
                setStep("step3");
                setPath([...path, "step3"]);
              }}
            >
              <img src={editApp} alt="" style={{ width: "20px" }} />
              <div style={{ paddingLeft: "10px" }}>Item 1 of Step 2</div>
            </div>
          </>
        );

      case "step3":
        return (
          <>
            <div className="sidebarTitle">This is the 3rd step </div>
            <div className="sidebarCard">
              <img src={editApp} alt="" style={{ width: "20px" }} />
              <div style={{ paddingLeft: "10px" }}>Item 1 of Step 3</div>
            </div>
          </>
        );

      default:
        return (
          <>
            <div className="sidebarTitle">What Would You Like To See? </div>
            <div
              style={{ overflowY: "scroll", height: window.innerHeight - 380 }}
            >
              {defaultCase.map((item) => {
                return (
                  <div
                    className="sidebarCard"
                    onClick={(e) => {
                      setStep(item.name);
                      setPath([...path, item.name]);
                    }}
                  >
                    <img src={item.icon} alt="" style={{ width: "20px" }} />
                    <div style={{ paddingLeft: "10px" }}>{item.name}</div>
                  </div>
                );
              })}
            </div>
          </>
        );
    }
  };

  return (
    <>
      <div
        style={{
          padding: "30px",
          height: "90px",
        }}
      >
        <div
          style={{
            display: "flex",
            alignItems: "center",
            justifyContent: "space-between",
          }}
        >
          <div style={{ display: "flex", alignItems: "center" }}>
            <img
              src={defaultImg}
              alt=""
              style={{ width: "40px", borderRadius: "100%" }}
            />
            <div style={{ paddingLeft: "10px" }}>
              <div style={{ fontSize: "22px" }}>Selected Item</div>

              <div
                className={
                  step === path[path.length]
                    ? "breadcrumbSelected"
                    : "breadcrumb"
                }
              >
                {path?.map((item, index) => {
                  return (
                    <span onClick={(e) => handleBreadcrumbClick(item)}>
                      {index !== 0 ? " > " : ""}&nbsp;{item}
                    </span>
                  );
                })}
              </div>
            </div>
          </div>

          <div style={{ display: "flex", alignItems: "center" }}>
            {path.length > 1 ? (
              <div
                className="backButton icon-imgs"
                onClick={(e) => handleBackStep()}
              >
                <img src={back} alt="" />
              </div>
            ) : (
              ""
            )}
            <div
              className="backButton icon-imgs"
              onClick={(e) => setShowSubDraw(false)}
              style={{
                marginLeft: "20px",
              }}
            >
              <img src={close} alt="" />
            </div>
          </div>
        </div>
        {conditionalSteps()}
      </div>
    </>
  );
};

export default About;
