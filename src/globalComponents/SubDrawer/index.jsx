import React, { useContext, useState } from "react";
import "./subdrawer.scss";
import { GlobalContex } from "../../globalContex";

import About from "./About";
import Actions from "./Actions";

function SubDrawer(tabSelected) {
  // console.log("subdrawer " + tabSelected);
  const { showSubDraw, setShowSubDraw } = useContext(GlobalContex);

  const [selectedMenu, setSelectedMenu] = useState("About");

  const conditionalData = () => {
    switch (selectedMenu) {
      case "About":
        return <About />;

      case "Actions":
        return <Actions />;

      default:
        break;
    }
  };

  return (
    <div
      className={showSubDraw ? "right-drawer-visible" : "right-drawer-hidden"}
      style={{ height: window.innerHeight - 123 }}
    >
      <div className="navs-disp">
        <div
          className={
            selectedMenu === "About" ? "navs-data active-tab" : "navs-data"
          }
        >
          <div onClick={(e) => setSelectedMenu("About")}>About</div>
        </div>
        <div
          className={
            selectedMenu === "Actions" ? "navs-data active-tab" : "navs-data"
          }
        >
          <div onClick={(e) => setSelectedMenu("Actions")}>Actions</div>
        </div>
      </div>
      {/* {thedata} */}
      {conditionalData()}
    </div>
  );
}

export default SubDrawer;
