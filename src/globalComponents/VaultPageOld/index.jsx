import React, { useEffect, useMemo, useState, useRef, useContext } from "react";

import classNames from "./bondsPage.module.scss";
import Skeleton from "react-loading-skeleton";
// import dashboard from "../../static/images/sidebarIcons/dash.svg";
import vaultss from "../../static/images/sidebarIcons/vaults.svg";
// import { FormatCurrency, FormatNumber } from "../../utils/FunctionTools";
import NavBar from "../NavBar";
import "./myStyle.css";
// import "./skeleton.css";
import "./vaults.scss";

import axios from "axios";
import FilterApps from "./FilterApps";
import FilterCoins from "./FilterCoins";
import { GlobalContex } from "../../globalContex";

// import { ReactComponent as IconClose } from "../../static/images/clipIcons/close.svg";

const tabs = ["All", "Active", "Expired", "Redeemed"];
function VaultPage() {
  const { bankerEmail, selectedCoin, setSelectedCoin } =
    useContext(GlobalContex);

  const [cardData, setCardData] = useState([]);
  const [tabSelected, setTabSelected] = useState("All");
  const [search, setSearch] = useState("");
  const [searchOn, setSearchOn] = useState(false);
  const [list, setList] = useState(false);

  const [filter1, setFilter1] = useState(false);

  const [openCoinFilter, setOpenCoinFilter] = useState(false);

  const [dataLoading, setDataLoading] = useState(false);
  const [selectedCard, setSelectedCard] = useState(null);
  const [selectedFilter1, setSelectedFilter1] = useState("crypto");

  const [allApps, setAllApps] = useState([]);
  const [showAppModal, setShowAppModal] = useState(false);
  const [selectedApp, setSelectedApp] = useState({
    app_code: "bankerapp",
    app_icon:
      "https://drivetest.globalxchange.io/gxsharepublic/?full_link=kamal.brain.stream/9aaf078edbeaf4e7c2339f654da23d89",
    app_name: "Banker",
    profile_id: "banku1f5ecf56a6ae9t1598888585397",
  });

  const [showCoinModal, setShowCoinModal] = useState(false);

  const [coinData, setCoinData] = useState([]);
  const [coinData1, setCoinData1] = useState([]);
  const [txnData, setTxnData] = useState([]);
  const [vaults, setVaults] = useState([]);

  const [showOtherFilters, setShowOtherFilters] = useState(false);

  const [skipState, setSkipState] = useState(0);
  const [countState, setCountState] = useState(0);
  const [endOfdata, setEndOfData] = useState(false);
  const [moreDataLoading, setMoreDataLoading] = useState(false);
  const [noTxns, setNoTxns] = useState(false);

  const listInnerRef = useRef();

  // const padNumber = (number) => {
  //   if (number < 999 && number > 1) {
  //     number = ("000" + number).slice(-6);
  //   }
  //   return number;
  // };

  useEffect(() => {
    console.log(selectedCoin, "ljwbdkjwbed");
  }, [selectedCoin]);

  const FormatNumber = (value, prec) => {
    return new Intl.NumberFormat("en-US", {
      maximumFractionDigits: prec,
      minimumFractionDigits: prec,
    }).format(isNaN(value) ? 0 : value);
  };

  const onScroll = () => {
    if (listInnerRef.current) {
      const { scrollTop, scrollHeight, clientHeight } = listInnerRef.current;
      // console.log("reached bottom", scrollTop, clientHeight, scrollHeight);
      if (Math.trunc(scrollTop) + clientHeight === scrollHeight) {
        if (!endOfdata && !moreDataLoading) {
          console.log(skipState, countState, "reached bottom");
          getNewTxns();
        }
      }
    }
  };

  const getNewTxns = () => {
    setMoreDataLoading(true);
    console.log(
      skipState,
      countState,
      Number(skipState) + Number(countState),
      "ndkjwblcwbelc"
    );
    axios
      .post(`https://comms.globalxchange.com/coin/vault/service/txns/get`, {
        app_code: selectedApp.app_code,
        email: bankerEmail,
        coin: selectedCoin.coinSymbol,
        skip: Number(skipState) + Number(countState),
      })
      .then((res) => {
        if (res.data.count === 0) {
          setEndOfData(true);
        }
        setTxnData([...txnData, ...res.data.txns]);
        setMoreDataLoading(false);
        setDataLoading(false);
        console.log(res.data, "qkdbwkdbw");
        setSkipState(res.data.skip);
        setCountState(res.data.count);
      });
  };

  useEffect(() => {
    axios
      .get(
        `https://comms.globalxchange.com/gxb/apps/registered/user?email=${bankerEmail}`
      )
      .then((res) => {
        setAllApps(res.data.userApps);
      });
  }, [bankerEmail]);

  const conditionalCoin = () => {
    if (selectedFilter1 === "forex" && selectedCoin !== null) {
      if (selectedCoin) {
        return selectedCoin.coinSymbol;
      } else {
        return "USD";
      }
    } else {
      if (selectedCoin) {
        return selectedCoin.coinSymbol;
      } else {
        return "BTC";
      }
    }
  };

  useEffect(() => {
    console.log(selectedCoin, "kwjbdkwe");
    if (selectedCoin !== null) {
      setTxnData([]);
      setDataLoading(true);
      axios
        .post(`https://comms.globalxchange.com/coin/vault/service/txns/get`, {
          app_code: selectedApp.app_code,
          email: bankerEmail,
          coin: conditionalCoin(),
        })
        .then((res) => {
          if (res.data.txns.length > 0) {
            setEndOfData(false);
            setTxnData([...res.data.txns]);
            setDataLoading(false);
            setSkipState(res.data.skip);
            setCountState(res.data.count);
            setNoTxns(false);
          } else {
            setDataLoading(false);
            setEndOfData(true);
            setNoTxns(true);
          }
        });
    }
  }, [selectedApp, selectedFilter1, selectedCoin, bankerEmail]);

  useEffect(() => {
    axios
      .post(`https://comms.globalxchange.com/coin/vault/service/coins/get`, {
        app_code: selectedApp.app_code,
        email: bankerEmail,
        type: selectedFilter1 === "crypto" ? "crypto" : "fiat",
        displayCurrency: "USD",
        // post_app_prices: true,
        with_balances: true,
      })
      .then((res) => {
        // console.log(res.data, "vaultsiuhiu");
        setCoinData(res.data.coins_data[0]);

        setVaults(res.data.coins_data);
        if (res.data.coins_data.length > 0) {
          setNoTxns(false);
          if (selectedFilter1 === "crypto") {
            setSelectedCoin(res.data.coins_data[0]);
          } else {
            setSelectedCoin(res.data.coins_data[1]);
          }
        } else {
          setSelectedCoin(null);
          setNoTxns(true);
        }
      });
  }, [selectedFilter1, selectedApp, bankerEmail]);

  const txnDetails = useMemo(() => {
    return txnData
      ? txnData.map((item, index) => {
          return (
            <>
              <div className="vaultsUsersGrid">
                <div style={{ display: "flex", alignItems: "center" }}>
                  <div>
                    {item.deposit ? (
                      <img
                        className="imgshadow"
                        src={
                          require("../../static/images/icons/withdraw.svg")
                            .default
                        }
                        alt=""
                        style={{ width: "37px" }}
                      />
                    ) : (
                      <img
                        className="imgshadow"
                        src={
                          require("../../static/images/icons/deposit.svg")
                            .default
                        }
                        alt=""
                        style={{ width: "37px" }}
                      />
                    )}
                  </div>
                  <div style={{ paddingLeft: "17px" }}>
                    <div
                      style={{
                        // fontSize: "13px",
                        fontWeight: "700",
                        display: "flex",
                        alignItems: "center",
                      }}
                    >
                      <div className="title">
                        {item.txn_metadata?.transfer_for
                          ? item.txn_metadata.transfer_for
                          : `${selectedCoin?.coinName} ${
                              item.deposit ? "Deposit" : "Withdraw"
                            }`}
                      </div>
                    </div>
                    <div
                      className="subtitle"
                      style={{
                        fontSize: "10px",
                        fontWeight: 400,
                        textAlign: "left",
                      }}
                    >
                      {new Date(item.date).toUTCString()}
                    </div>
                  </div>
                </div>

                <div
                  style={{
                    display: "flex",
                    flexDirection: "row",
                    alignItems: "center",
                  }}
                >
                  <div className="notes">Deposit</div>
                  <div style={{ paddingLeft: "10px" }}>
                    <img
                      style={{ cursor: "pointer" }}
                      src={
                        require("../../static/images/icons/edit.svg").default
                      }
                      onMouseOver={(e) =>
                        (e.target.src =
                          require("../../static/images/icons/edit_hover.svg").default)
                      }
                      onMouseOut={(e) =>
                        (e.target.src =
                          require("../../static/images/icons/edit.svg").default)
                      }
                      alt=""
                    />
                  </div>
                  <div style={{ paddingLeft: "10px" }}>
                    <img
                      style={{ cursor: "pointer" }}
                      src={
                        require("../../static/images/icons/arrow.svg").default
                      }
                      onMouseOver={(e) =>
                        (e.target.src =
                          require("../../static/images/icons/arrow_hover.svg").default)
                      }
                      onMouseOut={(e) =>
                        (e.target.src =
                          require("../../static/images/icons/arrow.svg").default)
                      }
                      alt=""
                    />
                  </div>
                  <div style={{ paddingLeft: "10px" }}>
                    <img
                      style={{ cursor: "pointer" }}
                      src={
                        require("../../static/images/icons/chat.svg").default
                      }
                      onMouseOver={(e) =>
                        (e.target.src =
                          require("../../static/images/icons/chat_hover.svg").default)
                      }
                      onMouseOut={(e) =>
                        (e.target.src =
                          require("../../static/images/icons/chat.svg").default)
                      }
                      alt=""
                    />
                  </div>
                </div>

                <div
                  className="title"
                  style={{
                    color: "#50C7AD",
                  }}
                >
                  {item.deposit ? FormatNumber(item.amount, 4) : "0.0000"}
                </div>
                <div className="title" style={{ color: "#EA0F0F" }}>
                  {item.withdraw ? FormatNumber(item.amount, 4) : "0.0000"}
                </div>
                <div
                  className="title"
                  style={{
                    textAlign: "right",
                    color: "#18191D",
                    fontWeight: 700,
                  }}
                >
                  {FormatNumber(item.updated_balance, 4)}
                </div>
              </div>
            </>
          );
        })
      : "";
  }, [txnData]);

  const tabComponent = useMemo(() => {
    return (
      <>
        <div
          style={{
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center",
            padding: "20px 30px",
          }}
        >
          {!selectedCoin ? (
            <div>&nbsp;</div>
          ) : (
            <div>
              <div className="vault-label">{selectedCoin?.coinName} Vault</div>
              <div style={{ display: "flex", alignItems: "baseline" }}>
                <div className="vault-price">
                  {selectedCoin
                    ? selectedFilter1 === "forex"
                      ? selectedCoin?.coinValue?.toFixed(2)
                      : selectedCoin?.coinValue?.toFixed(4)
                    : ""}
                </div>
                <div className="vault-price-subtitle">
                  &nbsp;$
                  {selectedCoin ? selectedCoin.coinValue_DC : "0.000"}
                </div>
              </div>
            </div>
          )}
          {/* {!dataLoading ? (
            !noTxns ? (
              <div>
                <div className="vault-label">
                  {selectedCoin?.coinName} Vault
                </div>
                <div style={{ display: "flex", alignItems: "baseline" }}>
                  <div className="vault-price">
                    {selectedCoin
                      ? selectedFilter1 === "forex"
                        ? selectedCoin.coinValue.toFixed(2)
                        : selectedCoin.coinValue.toFixed(4)
                      : ""}
                  </div>
                  <div className="vault-price-subtitle">
                    &nbsp;$
                    {selectedCoin ? selectedCoin.coinValue_DC : "0.000"}
                  </div>
                </div>
              </div>
            ) : (
              <div>
                <div className="vault-label">
                  {selectedCoin?.coinName} Vault
                </div>
                <div style={{ display: "flex", alignItems: "baseline" }}>
                  <div className="vault-price">0.000</div>
                  <div className="vault-price-subtitle">&nbsp;$ 0.000</div>
                </div>
              </div>
            )
          ) : (
            <div className="userDetail">
              <Skeleton className="name" width={250} />
              <Skeleton className="email" width={180} />
            </div>
          )} */}

          <div style={{ display: "flex", alignItems: "center" }}>
            <div
              className="dropdown-selector"
              onClick={(e) => setShowAppModal(true)}
            >
              <div style={{ display: "flex", alignItems: "center" }}>
                <img src={selectedApp.app_icon} alt="" width={15} />{" "}
                &nbsp;&nbsp;
                <div>{selectedApp.app_name}</div>
              </div>

              <img
                src={
                  require("../../static/images/icons/caret-down.svg").default
                }
                alt=""
              />
            </div>
            &nbsp;&nbsp;&nbsp;
            <div
              className="dropdown-selector"
              onClick={(e) => setShowCoinModal(true)}
            >
              <div style={{ display: "flex", alignItems: "center" }}>
                <img src={selectedCoin?.coinImage} alt="" width={17} />{" "}
                &nbsp;&nbsp;
                <div>{selectedCoin?.coinName}</div>
              </div>
              <img
                src={
                  require("../../static/images/icons/caret-down.svg").default
                }
                alt=""
              />
            </div>
            &nbsp;&nbsp;&nbsp;
            <button className="deposit-button">Deposit</button>
            &nbsp;&nbsp;&nbsp;
            <button className="withdraw-button">Withdraw</button>
          </div>
        </div>
        <div className="vaultsGrid">
          <div>Transactions</div>
          <div>&nbsp;</div>
          <div>Credit</div>
          <div>Debit</div>
          <div>Balance</div>
        </div>

        {showOtherFilters ? (
          <div
            style={{ padding: "20px", display: "flex", alignItems: "center" }}
          >
            <div
              style={{
                border: "solid 1px lightgray",
                borderRadius: "30px",
                padding: "10px 15px",
                color: "#18191D",
                fontWeight: 500,
                // fontSize: "10px",
              }}
            >
              + Add Directional Filter
            </div>
            &nbsp;&nbsp;&nbsp;
            <div
              style={{
                border: "solid 1px lightgray",
                borderRadius: "30px",
                padding: "10px 15px",
                color: "#18191D",
                fontWeight: 500,
              }}
            >
              + Add Categorical Filter
            </div>
            &nbsp;&nbsp;&nbsp;
            <div
              style={{
                border: "solid 1px lightgray",
                borderRadius: "30px",
                padding: "10px 15px",
                color: "#18191D",
                fontWeight: 500,
              }}
            >
              + Add Date Filter
            </div>
          </div>
        ) : (
          ""
        )}

        <div
          style={{
            // position: "fixed",
            // top: "290px",
            // bottom: "10px",
            // left: "395px",
            // right: "0px",
            overflowY: "scroll",
            height: "64vh",
            paddingBottom: "10vh",
          }}
          onScroll={onScroll}
          ref={listInnerRef}
        >
          {!noTxns ? (
            txnData.length > 0 ? (
              txnDetails
            ) : (
              Array(6)
                .fill("")
                .map((item, i) => {
                  return (
                    <div
                      className="vaultsUsersGrid post"
                      style={{
                        width: "100%",
                        borderBottom: "solid 0.5px #EEEEEE",
                      }}
                    >
                      <div
                        className="user"
                        key={i}
                        style={{ display: "flex", alignItems: "center" }}
                      >
                        <Skeleton
                          className="dp"
                          circle
                          width={37}
                          height={37}
                          style={{ marginRight: "20px" }}
                        />
                        <div className="userDetail">
                          <Skeleton
                            width={200}
                            style={{ marginBottom: "10px" }}
                          />
                          <Skeleton width={180} />
                        </div>
                      </div>
                      <div>
                        <Skeleton width={120} />
                      </div>
                      <div>
                        <Skeleton width={120} />
                      </div>
                      <div>
                        <Skeleton width={120} />
                      </div>
                    </div>
                  );
                })
            )
          ) : (
            <div
              style={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                height: "50vh",
              }}
            >
              <div
                style={{
                  fontSize: "20px",
                  fontWeight: "700",
                }}
              >
                There are no transactions
              </div>
            </div>
          )}
        </div>
      </>
    );
  }, [
    coinData,
    txnData,
    showOtherFilters,
    moreDataLoading,
    countState,
    skipState,
    selectedCoin,
    bankerEmail,
  ]);

  const ExtraFilter = () => {
    return (
      <div
        style={{
          display: "flex",
          justifyContent: "space-between",
          width: "250px",
          padding: "0px 20px",
          paddingRight: "20px",
        }}
      >
        <img
          onClick={(e) => setSelectedFilter1("crypto")}
          className="filter1"
          style={{
            cursor: "pointer",
            opacity: selectedFilter1 === "crypto" ? 1 : 0.4,
          }}
          src={require("../../static/images/filterIcons/1.svg").default}
          alt=""
          width={25}
        />
        <img
          onClick={(e) => setSelectedFilter1("forex")}
          className="filter1"
          style={{
            cursor: "pointer",
            opacity: selectedFilter1 === "forex" ? 1 : 0.4,
          }}
          src={require("../../static/images/filterIcons/2.svg").default}
          alt=""
          width={25}
        />
        <img
          className="filter1"
          style={{
            cursor: "pointer",
            opacity: selectedFilter1 === "abc" ? 1 : 0.4,
          }}
          src={require("../../static/images/filterIcons/3.svg").default}
          alt=""
          width={25}
        />
        <img
          className="filter1"
          style={{
            cursor: "pointer",
            opacity: selectedFilter1 === "abc" ? 1 : 0.4,
          }}
          src={require("../../static/images/filterIcons/4.svg").default}
          alt=""
          width={20}
        />
      </div>
    );
  };

  return (
    <>
      <NavBar
        logo={vaultss}
        name="Vaults"
        tabs={tabs}
        tabSelected={tabSelected}
        setTabSelected={setTabSelected}
        enabledFilters={[true, true, false, false, false]}
        customFilters={<ExtraFilter />}
      />

      {tabComponent}
      {showAppModal ? (
        <FilterApps
          onClose={() => setShowAppModal(false)}
          onSuccess={() => setShowAppModal(false)}
          showAppModal={showAppModal}
          setShowAppModal={setShowAppModal}
          selectedCoin={selectedCoin}
          setSelectedCoin={setSelectedCoin}
          selectedFilter1={selectedFilter1}
          setSelectedFilter1={setSelectedFilter1}
          allApps={allApps}
          setAllApps={setAllApps}
          selectedApp={selectedApp}
          setSelectedApp={setSelectedApp}
        />
      ) : (
        ""
      )}
      {showCoinModal ? (
        <FilterCoins
          onClose={() => setShowCoinModal(false)}
          onSuccess={() => setShowCoinModal(false)}
          showCoinModal={showCoinModal}
          setShowCoinModal={setShowCoinModal}
          selectedCoin={selectedCoin}
          setSelectedCoin={setSelectedCoin}
          selectedFilter1={selectedFilter1}
          setSelectedFilter1={setSelectedFilter1}
          allApps={allApps}
          setAllApps={setAllApps}
          selectedApp={selectedApp}
          setSelectedApp={setSelectedApp}
          vaults={vaults}
          setVaults={setVaults}
          setCoinData={setCoinData}
        />
      ) : (
        ""
      )}
    </>
  );
}

export default VaultPage;
