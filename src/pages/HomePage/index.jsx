// Package Imports
import React, { useContext, useEffect } from "react";
import { BrowserRouter, Routes, Route, Navigate } from "react-router-dom";
import axios from "axios";

// CSS Imports

// Context Imports
import { GlobalContex } from "../../globalContex";

// Image Imports

//cPage/Component Imports
import MarketsVerse from "../../Apps/MarketsVerse";
import Banker from "../../Apps/Banker";
import Capitalized from "../../Apps/Capitalized";
import Creating from "../../Apps/Creating";
import FundManagers from "../../Apps/FundManagers";
import OTCDesks from "../../Apps/OTCDesks";
import Terminals from "../../Apps/Terminals";
import TaxChains from "../../Apps/TaxChains";
import InstaLegal from "../../Apps/InstaLegal";
import Accounts from "../../Apps/Accounts";
import Agency from "../../Apps/Agency";
import CA from "../../Apps/CA";
import Accountants from "../../Apps/Accountants";
import Firms from "../../Apps/Firms";
import Cabinets from "../../Apps/Cabinet";
import Chats from "../../Apps/Chats";

const HomePage = () => {
  const { loginData, setBankerTag, setBanker } = useContext(GlobalContex);

  useEffect(() => {
    axios
      .get(
        "https://teller2.apimachine.com/banker/details",

        {
          headers: {
            email: loginData.user.email,
            token: loginData.idToken,
          },
        }
      )
      .then((res) => {
        if (!localStorage.getItem("bankerTag")) {
          console.log(res.data, "jhffjy");
          localStorage.setItem("bankerTag", res.data.data.bankerTag);
          setBankerTag(res.data.data.bankerTag);

          localStorage.setItem("banker", JSON.stringify(res.data.data));
          setBanker(res.data.data);
        }
      });
  }, []);

  return (
    <>
      <Routes>
        <Route path="TaxChains" element={<TaxChains />} />
        <Route path="InstalLegal" element={<InstaLegal />} />
        <Route path="Accounts" element={<Accounts />} />
        <Route path="Cabinets" element={<Cabinets />} />
        <Route path="Chats" element={<Chats />} />
        <Route path="Firms.App" element={<Firms />} />
        <Route path="Lawyer.Agency" element={<Agency />} />
        <Route path="Accountants" element={<Accountants />} />
        <Route path="CA" element={<CA />} />
        <Route path="MarketsVerse" element={<MarketsVerse />} />
        <Route path="Banker" element={<Banker />} />
        <Route path="Capitalized" element={<Capitalized />} />
        <Route path="Creating" element={<Creating />} />
        <Route path="FundManagers" element={<FundManagers />} />
        <Route path="OTCDesks" element={<OTCDesks />} />
        <Route path="Terminals" element={<Terminals />} />
      </Routes>
    </>
  );
};

export default HomePage;
