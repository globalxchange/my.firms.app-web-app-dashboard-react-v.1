// Package Imports
import React, { useEffect, useState, useContext } from "react";
import axios from "axios";

// CSS Imports
import classNames from "./signInPage.module.scss";
import "./mobile.scss";
import "./login.scss";

// Context Imports
import { GlobalContex } from "../../globalContex";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

// Image Imports

//cPage/Component Imports
// import LoadingAnimation from "../../components/LoadingAnimation";
import GlobalSidebar from "../../globalComponents/GlobalSidebar";
import LoadingAnimation from "../../Apps/LoadingAnimations/Loading/loadings"
import FirmsLoader from "../../Apps/LoadingAnimations/FirmLoading/FirmsAnim"

const SignInPage = () => {
  const {
    selectedApp,
    setSelectedApp,

    loginData,
    setLoginData,
    globalMenu,
    setBankerEmail
  } = useContext(GlobalContex);
  const [focused, setFocused] = useState(false);
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [twoFaPin, setTwoFaPin] = useState("");
  const [footerLoading, setFooterLoading] = useState(false);
  const [loading, setLoading] = useState(false);

  const handleLogin = async () => {
    console.log("inhandlelogin");
    setLoading(true);
    if (selectedApp.appName === "Accountants" || selectedApp.appName === "Firms.App") {
      if (email && password) {
        const loginData = await axios.post(
          `https://gxauth.apimachine.com/gx/user/login`,
          {
            email: email,
            password: password,
          }
        );

        if (loginData.data.status) {
          console.log("inhandlelogin", loginData);
          try {
            setLoginData(loginData.data);
            localStorage.setItem("loginData", JSON.stringify(loginData.data));
            localStorage.setItem("bankerEmailNew", loginData?.data?.user.email);
            setBankerEmail(loginData?.data?.user?.email);
            setLoading(false);
          } catch (error) {
            console.log(error);
            setLoading(false);
          }
        } else {
          setLoading(false)
          toast.error(loginData.data.message)
        }
      } else {
        setLoading(false)
        toast.error("please enter the details")
      }
    } else {
      setLoading(false)
      toast.info("Page Under Construction")
    }
  };

  return (
    <>
      <div style={{ display: "flex" }}>
        <GlobalSidebar globalMenu={globalMenu} />
        <div
          style={{
            display: "grid",
            gridTemplateColumns: "1fr 1fr",
            width: "100%",
          }}
        >
          <div
            style={{
              display: "flex",
              flexDirection: "column",
              justifyContent: "center",

              paddingLeft: "15%",
            }}
          >
            {loading ? selectedApp?.appName === "Firms.App" ? <FirmsLoader/> : <LoadingAnimation /> : ""}

            <>
              <div style={{ width: "50%" }}>
                <div style={{ width: "80%" }}>
                  <img
                    style={{ width: "100%" }}
                    src={selectedApp?.appFullLogo}
                    alt=""
                    className={classNames.logo}
                  />
                </div>
                <div style={{ paddingTop: "50px", paddingBottom: "60px" }}>
                  <div>
                    <input
                      value={email}
                      onChange={(e) => setEmail(e.target.value)}
                      className="inputLogin"
                      type="text"
                      placeholder="Email"
                    />
                  </div>
                  <div style={{ paddingTop: "50px" }}>
                    <input
                      value={password}
                      onChange={(e) => setPassword(e.target.value)}
                      className="inputLogin"
                      type="password"
                      placeholder="Password"
                    />
                  </div>
                </div>
              </div>
              <div
                onClick={handleLogin}
                className="btnLogin"
                style={{ background: selectedApp?.appColor }}
              >
                {loading ? "Logging In....." : "Login"}

              </div>
            </>
          </div>

          <div
            className="heroText"
            style={{
              background: selectedApp?.appName === "TaxChains" ? "#1F304F": selectedApp?.appColor,
              color: "white",
              display: "flex",
              flexDirection: "column",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <div>
              Don't Have A<br /> {selectedApp?.appName}
              <br /> Account?
              <div className="clickButton" style={{ marginTop: "70px" }}>
                Click Here
              </div>
            </div>
          </div>
        </div>
        <ToastContainer/>
      </div>
    </>
  );
};

export default SignInPage;
