const Images = {
  accountsLogo: require("./accounts-logo.svg").default,
  accountsLogoWhite: require("./accounts-logo-white.svg").default,
  lady: require("./lady.png").default,
  banker: require("./banker.svg").default,
  connecting: require("./connecting.svg").default,
  manWithTelescope: require("./man-with-telescope.svg").default,
  moneyAndCoins: require("./money-and-coins.svg").default,
  phone: require("./phone.svg").default,
  web: require("./web.svg").default,
  plus: require("./plus.svg").default,
  settings: require("./settings.svg").default,
  cancel: require("./cancel.svg").default,
  mainLogo: require("./main-logo.svg").default,
  vault: require("./vault.svg").default,
  moneyMarket: require("./money-market.svg").default,
  bonds: require("./bonds.svg").default,

  agStore: require("./ag-store.svg").default,
  refresh: require("./refresh.svg").default,
  theatre: require("./theatre.svg").default,
  chats: require("./chats.svg").default,
  accountants: require("./accounts.svg").default,
  lock: require("./lock.svg").default,
  share: require("./share.svg").default,
  download: require("./download.svg").default,

  csv: require("./csv.svg").default,
  excel: require("./excel.svg").default,
  pdf: require("./pdf.svg").default,
  png: require("./png.svg").default,
  mainLogo: require("./main-logo.svg").default,
  mainLogoFull: require("./main-logo-full.svg").default,

  taxchainicon: require("./taxchainicon.svg").default,
  judgeicon: require("./judgeicon.svg").default,
  devsicon: require("./devsicon.svg").default,
  justiceicon: require("./justiceicon.svg").default,
  elephanticon: require("./elepicon.svg").default,

  legacyicon: require("./legalicon.svg").default,
  agencyicon: require("./agencyicon.svg").default,
  accountsicon: require("./accountsicon.svg").default,
  CAicon: require("./CAlogo.svg").default,

  icon1: require("./FirmsLogo.svg").default,
  icon2: require("./accicon2.svg").default,
  icon3: require("./LawyerLogo.svg").default,
  icon4: require("./CabinetLogo.svg").default,
  icon5: require("./ChatLogo.svg").default,

  FirmsFullLogo: require("./FirmsFullLogo.svg").default,
  LawyerFullLogo: require("./LawyerFullLogo.svg").default,
  CabinetFullLogo: require("./CabinetFullLogo.svg").default,
  ChatsFullLogo: require("./ChatsFullLogo.svg").default,

  directIcon: require("./directicon.svg").default



};

export default Images;
